package com.spring.mvc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class HttpWebUtil {
	public static void main(String[] args) {
		String strUrl = "http://news.163.com/special/0001220O/news_json.js";
		String html = getHtml(strUrl,"gb2312");
		System.out.println();
		
		String jsonStr = html.substring(html.indexOf("=")+1,html.lastIndexOf(";"));
		System.out.println(jsonStr);
		
		
		JSONObject jsonObj = JSON.parseObject(jsonStr);
		JSONArray categoryArray = jsonObj.getJSONArray("category");
		for(int i = 0; i < categoryArray.size();i++){
			JSONObject obj = (JSONObject) categoryArray.get(i);
			System.out.println(obj.get("n")+":"+obj.get("l"));
		}
		System.out.println("一共："+categoryArray.size()+"个分类");
		
		JSONArray newsArray = jsonObj.getJSONArray("news").getJSONArray(0);
		for(int i = 0; i < newsArray.size(); i++){
			JSONObject obj = (JSONObject) newsArray.get(i);
			System.out.println("标题: "+obj.get("t"));
			System.out.println("时间: "+obj.get("p"));
			System.out.println("链接: "+obj.get("l"));
			System.out.println();
		}
		System.out.println("一共："+newsArray.size()+"条新闻");
		
//		System.out.println(categoryArray);
//		System.out.println(newsArray);
	}
	
	public static String getHtml(String strUrl,String charset){
		StringBuffer sb = new StringBuffer();
		try {
			// 创建一个url对象来指向 该网站链接 括号里()装载的是该网站链接的路径
			// 更多可以看看 http://wenku.baidu.com/view/8186caf4f61fb7360b4c6547.html
			URL url = new URL(strUrl);
			// InputStreamReader 是一个输入流读取器 用于将读取的字节转换成字符
			// 更多可以看看 http://blog.sina.com.cn/s/blog_44a05959010004il.html
			InputStreamReader isr = new InputStreamReader(url.openStream(),charset); // 统一使用utf-8 编码模式
			// 使用 BufferedReader 来读取 InputStreamReader 转换成的字符
			BufferedReader br = new BufferedReader(isr);
			// 如果 BufferedReader 读到的内容不为空
			String line = "";
			while ((line  = br.readLine()) != null) {
				// 则打印出来 这里打印出来的结果 应该是整个网站的
//				System.out.println(line);
				sb.append(line+"\n");
			}
			br.close(); // 读取完成后关闭读取器
		} catch (IOException e) {
			// 如果出错 抛出异常
			e.printStackTrace();
		}
		return sb.toString();
	}
}
