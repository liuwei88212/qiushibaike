package com.spring.mvc.news;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.spring.mvc.util.HttpWebUtil;

/**
 * 采集新闻内容
 * 
 * 新浪、网易
 * 
 * @author Administrator
 *
 */
public class NewsUtil {
	public static void main(String[] args) {
		/**
		 * 新浪新闻
		 * http://roll.news.sina.com.cn/s/channel.php?ch=02#col=64&spec=&type=&ch=02&k=&offset_page=0&offset_num=0&num=60&asc=&page=1
		 */
//		String sinaUrl = "http://roll.news.sina.com.cn";
//		getSinaList(sinaUrl, "gb2312");
		
		String url163 = "http://news.163.com/latest/";
		get163List(url163, "gb2312");
	}
	
	public static void getSinaList(String url,String charset){
		Document doc = HttpWebUtil.getDoc(url,charset);
		Element ul = doc.getElementById("d_list");
		for(Element li:ul.getElementsByTag("li")){
			String title = li.select("span.c_tit").select("a").html();
			System.out.println("tilte:"+title);
			
			String titleUrl = li.select("span.c_tit").select("a").attr("href");
			System.out.println("titleUrl:"+titleUrl);
			
			String time = li.select("span.c_time").html();
			System.out.println("time:"+time);
			System.out.println();
		}
	}
	
	public static void get163List(String url,String charset){
		Document doc = HttpWebUtil.getDoc(url,charset);
		Element ul = doc.select("div.list_txt").first();
		for(Element li:ul.getElementsByTag("li")){
			String time = li.select("span").html();
			System.out.println("time:"+time);
			
			String title = li.select("a").get(1).html();
			System.out.println("title:"+title);
			
			String titleUrl = li.select("a").get(1).attr("href");
			System.out.println("titleUrl:"+titleUrl);
			System.out.println();
		}
	}
}	
