package com.spring.mvc.news.bean;

public class New {
	private int c;
	private String t;
	private String l;
	private String p;
	
	
	public New(){}
	
	public New(int c,String t,String l ,String p){
		this.c = c;
		this.t = t;
		this.l = l;
		this.p = p;
	}
	
	public int getC() {
		return c;
	}
	public void setC(int c) {
		this.c = c;
	}
	public String getT() {
		return t;
	}
	public void setT(String t) {
		this.t = t;
	}
	public String getL() {
		return l;
	}
	public void setL(String l) {
		this.l = l;
	}
	public String getP() {
		return p;
	}
	public void setP(String p) {
		this.p = p;
	}
}
