package com.spring.mvc.news.bean;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.spring.mvc.common.NewsJson;

public class NetJsonBean {
	public static void main(String[] args) {
		List<Category> category = new ArrayList<Category>();
		category.add(new Category("国内", "http://news.163.com/domestic/"));
		category.add(new Category("国际", "http://news.163.com/world/"));
		
		List<New> news = new ArrayList<New>();
		news.add(new New(0,"任志强自曝是北京4号线第2大股东:十几年没利益","http://news.163.com/14/1120/22/ABHEINUK00014JB6.html","2014-11-20 22:34:00"));
		news.add(new New(1,"任志强自曝是北京4号线第2大股东:十几年没利益","http://news.163.com/14/1120/22/ABHEINUK00014JB6.html","2014-11-20 22:34:00"));
		news.add(new New(2,"任志强自曝是北京4号线第2大股东:十几年没利益","http://news.163.com/14/1120/22/ABHEINUK00014JB6.html","2014-11-20 22:34:00"));
		
		NetJsonBean jsonBean = new NetJsonBean();
		jsonBean.setCategory(category);
		jsonBean.setNews(news);
		
		System.out.println(JSON.toJSONString(jsonBean));		
	}
	
	public List<Category> category;
	
	public List<New> news;

	public List<Category> getCategory() {
		return category;
	}

	public void setCategory(List<Category> category) {
		this.category = category;
	}

	public List<New> getNews() {
		return news;
	}

	public void setNews(List<New> news) {
		this.news = news;
	}
}
