package com.spring.mvc.news.bean;

public class Category {
	private String n;
	private String l;
	
	public Category(){}
	
	public Category(String n,String l){
		this.n = n;
		this.l = l;
	}
	
	public String getN() {
		return n;
	}
	public void setN(String n) {
		this.n = n;
	}
	public String getL() {
		return l;
	}
	public void setL(String l) {
		this.l = l;
	}
	
	
}
