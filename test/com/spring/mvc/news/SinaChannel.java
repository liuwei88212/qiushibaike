package com.spring.mvc.news;

import java.util.HashMap;
import java.util.Map;

public class SinaChannel {
	
	public static Map<String,String> channelMap = new HashMap<String, String>();
	
	static{
		channelMap.put("01", "89");//全部
		channelMap.put("01", "90");//国内
		channelMap.put("01", "91");//国际
		channelMap.put("01", "92");//社会
		channelMap.put("01", "93");//军事
		channelMap.put("01", "94");//体育
		channelMap.put("01", "95");//娱乐
		channelMap.put("01", "96");//科技
		channelMap.put("01", "97");//财经
		channelMap.put("01", "98");//股市
		channelMap.put("01", "99");//美股
		
		
		channelMap.put("02", "64");//全部
		channelMap.put("02", "65");//中国足球
		channelMap.put("02", "66");//国际足球
		channelMap.put("02", "67");//综合体育
		channelMap.put("02", "181");//NBA
		channelMap.put("02", "180");//中国篮球
		
		channelMap.put("02", "70");//全部
	}
}
