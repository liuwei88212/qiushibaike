package com.spring.mvc.news;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Test {
	public static void getNewsContent(){
        StringBuffer html =new StringBuffer();
        try{
            //链接163新闻
            Document doc = Jsoup.connect("http://news.163.com").get();
            //和jquery语法类似 找到class=JS_NTES_LOG_FE 下面所有的链接  就是这么简单
            Elements links = doc.select(".JS_NTES_LOG_FE a");
            int j =0 ;
            for (Element link : links) {
              //取得链接
              String linkHref = link.attr("href");
              //取得文本内容
              String linkText = link.text();
              System.out.println(linkText+linkHref);
              //拼接字符串
              html.append("<li><a href=\""+linkHref+"\" target=\"_blank\" " +
                    "title=\""+linkText+"\">"+linkText+"</a></li>");
              j++;
            } 
        }catch(Exception e){
            //异常
        }
        System.out.println(html);
    }

    public static void main(String[] args) throws IOException {
        getNewsContent();
    }
}
