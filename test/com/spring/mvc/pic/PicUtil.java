package com.spring.mvc.pic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.spring.mvc.common.FileUtil;
import com.spring.mvc.util.HttpWebUtil;

public class PicUtil {
	public static void main(String[] args){
//		String url = "http://www.2222be.com/artlist/23.html";
//		getImages(url, "utf-8");
//		getHtml("http://www.2222be.com/arts/1940.html","utf-8");
		for(int i=1;i<33;i++){
			String url = "http://www.2222be.com/artlist/23-"+i+".html";
			getImages(url, "utf-8");
		}
	}
	
	public static  void getHtml(String url,String charset){
		String content  =  "";
		try {
			content = HttpWebUtil.getWebContent(url, charset);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(content);
	}
	
	public static void getImages(String url,String charset){
		String projectRoot = url.substring(url.indexOf(".")+1,url.lastIndexOf("."));
		String root = url.substring(0,url.indexOf("com/")+3);
		System.out.println(url);
		Document doc = HttpWebUtil.getDoc(url,charset);
		Element title = doc.getElementsByTag("title").first();
		System.out.println(title);
		Elements lis = doc.select("li.name");
		for(Element li : lis){
			String href = li.select("a").attr("href");
			String name = li.select("a").html();
			System.out.println("名称："+name);
			System.out.println("链接 ："+(root+href));
			
			String imgDir = "D:/pic/"+projectRoot;
			
			doc = HttpWebUtil.getDoc((root+href),charset);
			Element div = doc.getElementById("postmessage");
			for(Element img:div.getElementsByTag("img")){
				String src = img.attr("src");
				String imgName = src.substring(src.lastIndexOf("/")+1);
				String imgPath = imgDir+"/"+imgName;
				if(!new File(imgPath).exists()){
					try {
						FileUtil.createFile(imgPath);
						System.out.println("->"+src+" ...");
						URL urls = new URL(src);
						InputStream in = urls.openConnection().getInputStream();
						//边读边写  
						FileOutputStream out = new FileOutputStream(imgPath);
						byte[] buff = new byte[1024];
						int i = 0;
						while((i = in.read())!=-1){
							//每读取一次，即写入文件输出流，读了多少，就写多少
							out.write(i); 
//							out.write(buff,0,i);  
						}
						out.close();
						System.out.println("【成功】->"+imgPath+"\n");
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			}
		}
	}
}
