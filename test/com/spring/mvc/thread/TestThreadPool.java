package com.spring.mvc.thread;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TestThreadPool {
	public static void main(String[] args) {

		System.out.println("main thread started!");
		/*
		 * public ThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long
		 * keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue)
		 */
		ThreadPoolExecutor threadPool = new ThreadPoolExecutor(5, 8, 1, TimeUnit.SECONDS,
				new LinkedBlockingQueue<Runnable>());
		for (int i = 0; i < 10; i++) {
			threadPool.execute(new Task(i));
		}
		threadPool.shutdown(); // 关闭后不能加入新线程，队列中的线程则依次执行完
		while(true){
			if(threadPool.isTerminated()){
				System.out.println("main thread end!");	
				break;	
			}
		}
	}
}

class Task implements Runnable {
	int count;

	Task(int i) {
		this.count = i;
	}

	public void run() {
		try {
			System.out.println("in thread " + count);
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("    thread " + count + " end!");
	}
}
