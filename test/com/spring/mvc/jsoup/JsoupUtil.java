package com.spring.mvc.jsoup;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.spring.mvc.common.FileUtil;
import com.spring.mvc.util.HttpWebUtil;

/**
 * JSOUP 采集网页
 * 
 * 下载HTML、CSS、HTML
 * 
 * @author Administrator
 *
 */
public class JsoupUtil {
	
	private static String root = "D:/JAVA采集网页";
	//http://news.163.com/latest
	private static String webUrl = "http://www.qiushibaike.com";
	
	public static void main(String[] args) throws Exception {
		getHtml(webUrl, "utf-8");
		getImages(webUrl, "utf-8");
		getScript(webUrl, "utf-8");
		getCss(webUrl, "utf-8");
	}
	
	public static void getHtml(String strUrl,String charset){
		String projectRoot = strUrl.substring(strUrl.indexOf(".")+1,strUrl.lastIndexOf(".com"));
		try {
			URL url = new URL(strUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			
			File scriptFile = new File(root+"/"+projectRoot+"/index.html");
			FileUtil.createFile(scriptFile);
			
			InputStream in = conn.getInputStream();
			//边读边写
			FileOutputStream out = new FileOutputStream(scriptFile);
			byte[] buff = new byte[1024];
			int i = 0;
			while((i = in.read(buff))!=-1){
				//每读取一次，即写入文件输出流，读了多少，就写多少  
				out.write(buff,0,i);  
			}
			out.close();
			
			System.out.println(strUrl+" ->"+scriptFile +"->【成功】");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void getImages(String strUrl,String charset){
		String projectRoot = strUrl.substring(strUrl.indexOf(".")+1,strUrl.lastIndexOf(".com"));
		String wwwRoot = strUrl.substring(0,strUrl.indexOf("com")+3);
		
		Document doc = HttpWebUtil.getDoc(strUrl,charset);
		Elements scriptTags = doc.getElementsByTag("img");
		for(Element script:scriptTags){
			String scriptUrl = script.attr("src");
			if(scriptUrl!=null && !"".equals(scriptUrl) && !scriptUrl.startsWith("http")){
				// a/b
				// ./a/b
				if(scriptUrl.startsWith("./")){
					scriptUrl = scriptUrl.substring(scriptUrl.indexOf("/")+1);	
				}
				scriptUrl = wwwRoot+"/"+scriptUrl;
			}
			
			if(!"".equals(scriptUrl) && !scriptUrl.equals(strUrl)){
				InputStream in = null;
				try {
					URL url = new URL(scriptUrl);
					in = url.openConnection().getInputStream();
				
					String tempPath = scriptUrl.substring(scriptUrl.indexOf("//")+2);
					String filePath = tempPath.substring(tempPath.indexOf("/")+1,tempPath.lastIndexOf("?")>-1?tempPath.lastIndexOf("?"):tempPath.length());
					
					File scriptFile = new File(root+"/"+projectRoot+"/"+filePath);
					FileUtil.createFile(scriptFile);
					
					//边读边写
					FileOutputStream out = new FileOutputStream(scriptFile);
					byte[] buff = new byte[1024];
					int i = 0;
					while((i = in.read(buff))!=-1){
						//每读取一次，即写入文件输出流，读了多少，就写多少  
						out.write(buff,0,i);  
					}
					out.close();
					System.out.println("img: "+scriptUrl+" ->"+scriptFile +"->【成功】");
				} catch (MalformedURLException e) {
					System.err.println("img: "+scriptUrl+"->【失败】");
				} catch (IOException e) {
					System.err.println("img: "+scriptUrl+"->【失败】");
				}
			}
		}
	}
	
	public static void getCss(String strUrl,String charset){
		String projectRoot = strUrl.substring(strUrl.indexOf(".")+1,strUrl.lastIndexOf(".com"));
		String wwwRoot = strUrl.substring(0,strUrl.indexOf("com")+3);
		
		Document doc = HttpWebUtil.getDoc(strUrl,charset);
		Elements scriptTags = doc.getElementsByTag("link");
		for(Element script:scriptTags){
			String scriptUrl = script.attr("href");
			if(scriptUrl.startsWith("./")){
				scriptUrl = scriptUrl.substring(scriptUrl.indexOf("/")+1);	
			}
			if(scriptUrl!=null && !"".equals(scriptUrl) && !scriptUrl.startsWith("http")){
				scriptUrl = wwwRoot+"/"+scriptUrl;
			}
			
			if(!"".equals(scriptUrl) && !scriptUrl.equals(strUrl)){
				InputStream in = null;
				try {
					URL url = new URL(scriptUrl);
					in = url.openConnection().getInputStream();
				
					String tempPath = scriptUrl.substring(scriptUrl.indexOf("//")+2);
					String filePath = tempPath.substring(tempPath.indexOf("/")+1,tempPath.lastIndexOf("?")>-1?tempPath.lastIndexOf("?"):tempPath.length());
					
					File scriptFile = new File(root+"/"+projectRoot+"/"+filePath);
					FileUtil.createFile(scriptFile);
					
					//边读边写
					FileOutputStream out = new FileOutputStream(scriptFile);
					byte[] buff = new byte[1024];
					int i = 0;
					while((i = in.read(buff))!=-1){
						//每读取一次，即写入文件输出流，读了多少，就写多少  
						out.write(buff,0,i);  
					}
					out.close();
					
					System.out.println("css: "+scriptUrl+" ->"+scriptFile +"->【成功】");
				} catch (MalformedURLException e) {
					System.err.println("css: "+scriptUrl+"->【失败】");
				} catch (IOException e) {
					System.err.println("css: "+scriptUrl+"->【失败】");
				}
			}
		}
	}
	
	public static void getScript(String strUrl,String charset){
		String projectRoot = strUrl.substring(strUrl.indexOf(".")+1,strUrl.indexOf(".com"));
		String wwwRoot = strUrl.substring(0,strUrl.indexOf("com")+3);
		
		Document doc = HttpWebUtil.getDoc(strUrl,charset);
		Elements scriptTags = doc.getElementsByTag("script");
		for(Element script:scriptTags){
			String scriptUrl = script.attr("src");
			if(scriptUrl!=null && !"".equals(scriptUrl) && !scriptUrl.startsWith("http")){
				if(scriptUrl.startsWith("./")){
					scriptUrl = scriptUrl.substring(scriptUrl.indexOf("/")+1);	
				}
				
				scriptUrl = wwwRoot+"/"+scriptUrl;
			}
			
			if(!"".equals(scriptUrl) && !scriptUrl.equals(strUrl)){
				try {
					URL url = new URL(scriptUrl);
					InputStream in = url.openConnection().getInputStream();
					
					
					String tempPath = scriptUrl.substring(scriptUrl.indexOf("//")+2);
					String filePath = tempPath.substring(tempPath.indexOf("/")+1,tempPath.lastIndexOf("?")>-1?tempPath.lastIndexOf("?"):tempPath.length());
					
					
					File scriptFile = new File(root+"/"+projectRoot+"/"+filePath);
					FileUtil.createFile(scriptFile);
					
					//边读边写
					FileOutputStream out = new FileOutputStream(scriptFile);
					byte[] buff = new byte[1024];
					int i = 0;
					while((i = in.read(buff))!=-1){
						//每读取一次，即写入文件输出流，读了多少，就写多少  
						out.write(buff,0,i);  
					}
					out.close();
					
					System.out.println("script: "+scriptUrl+" ->"+scriptFile +"->【成功】");
				}  catch (MalformedURLException e) {
					System.err.println("script: "+scriptUrl+"->【失败】");
				} catch (IOException e) {
					System.err.println("script: "+scriptUrl+"->【失败】");
				}
				
			}
		}
	}
}
