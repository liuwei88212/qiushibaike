package com.spring.mvc.js;

import java.io.Reader;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import org.apache.ibatis.io.Resources;
import sun.org.mozilla.javascript.internal.NativeObject;

import com.spring.mvc.util.Base64Util;

public class InvokeJsInJava {
	public static void main(String[] args) {
		InvokeJsInJava.test2();
	}

	public static void test1() {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("javascript");
		try {
			String str = "1111111111";
			Double d = (Double) engine.eval(str);
			Integer i = d.intValue();
			System.out.println(i);
		} catch (ScriptException ex) {
			ex.printStackTrace();
		}
	}

	public static void test2() {
		// 获得一个JavaScript脚本引擎，也可以是ECMAScript脚本引擎
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		String titlescript = "function sayTitle() {"
				+ " println('使用javax.script调用JS脚本里的方法');"
				+ " println('--------------------------------------------------');"
				+ "}";
		try {
			// 调用内部脚本执行-----------------------------------------
			engine.eval(titlescript);
			// 转换为Invocable
			Invocable invocableEngine = (Invocable) engine;
			// 不带参数调用sayTitle方法
			invocableEngine.invokeFunction("sayTitle");

			// 调用外部脚本执行------------------------------------------
			// 创建JS文件的File对象，并读入流
			String resource = "js/Base64.js";
			Reader reader = Resources.getResourceAsReader(resource);
			
			// 开始执行Base64.js里的程序
			engine.eval(reader);
			
			NativeObject nativeObj = (NativeObject) engine.get("Base64");
			String s = (String) invocableEngine.invokeMethod(nativeObj, "encode", "123");
			
			Base64Util base64 = invocableEngine.getInterface(Base64Util.class);
			// 带参数调用sayHello方法
			invocableEngine.invokeFunction("Base64.encode", "123123");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
