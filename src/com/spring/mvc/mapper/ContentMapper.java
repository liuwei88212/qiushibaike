package com.spring.mvc.mapper;

import com.spring.mvc.entity.Content;
import com.spring.mvc.entity.ContentExample;
import java.util.List;

public interface ContentMapper {
	
    List<Content> selectByExample(ContentExample example);
    
    int countByExample(ContentExample example);
    
    void insertBatch(List<Content> list);

    int deleteDuplicate();
}