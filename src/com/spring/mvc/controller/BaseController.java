package com.spring.mvc.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;

public class BaseController extends HttpServlet{
	private static final long serialVersionUID = -7030880638501173219L;
	
	protected static final int DEFAULT_NUMPERPAGE = 10;//每页展示的条数
	
	public void service(ServletRequest req, ServletResponse res)
			throws ServletException, IOException {
		super.service(req, res);
	}

	public void init() throws ServletException {
		super.init();
	}

}
