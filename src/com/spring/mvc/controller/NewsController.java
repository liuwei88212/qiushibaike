package com.spring.mvc.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;

import com.spring.mvc.entity.News;
import com.spring.mvc.service.NewsService;

public class NewsController extends BaseController{
	private static final long serialVersionUID = -5829564857858113925L;
	private static final String DEFAULT_ORDER = "id desc";
	private NewsService newsService;
	public void init() throws ServletException {
		newsService = new NewsService(); 
    }
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String method = request.getParameter("method");
		if("list".equals(method)){
			list(request, response);
		}else if("load".equals(method)){
			load(request, response);
		}else{
			list(request, response);
		}
	}
	
	public void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String pageNumStr = request.getParameter("pageNum");
		String from = request.getParameter("from");
    	Integer pageNum = (pageNumStr==null || "".equals(pageNumStr)) ? 1:Integer.parseInt(pageNumStr);
		Page<News> page = newsService.selectByPage(pageNum,DEFAULT_NUMPERPAGE,DEFAULT_ORDER);
		request.setAttribute("page", page);
		request.setAttribute("from", from);
		request.getRequestDispatcher("inspect.jsp").forward(request, response);
	}
	
	public void load(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String pageNumStr = request.getParameter("pageNum");
		String from = request.getParameter("from");
    	Integer pageNum = (pageNumStr==null || "".equals(pageNumStr)) ? 1:Integer.parseInt(pageNumStr);
		Page<News> page = newsService.selectByPage(pageNum,DEFAULT_NUMPERPAGE,DEFAULT_ORDER);
		request.setAttribute("page", page);
		request.setAttribute("from", from);
		request.getRequestDispatcher("inspect.load.jsp").forward(request, response);
	}
}
