package com.spring.mvc.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;

import com.spring.mvc.entity.Content;
import com.spring.mvc.entity.User;
import com.spring.mvc.service.UserService;

public class UserController extends BaseController{
	private static final long serialVersionUID = 1L;
	
	private UserService userService ;
	
	public void init() throws ServletException {
		userService = new UserService(); 
    }
	
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String method = request.getParameter("method");
		if("detail".equals(method)){
			detail(request, response);
		}
	}
	private void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String from = request.getParameter("from");
		long userId = Long.parseLong(request.getParameter("userId"));
		User user = userService.selectByUser(userId);
		Page<Content> page = userService.selectByUser(1,10000,userId);
		request.setAttribute("page", page);
		request.setAttribute("user", user);
		request.setAttribute("from", from);
		request.getRequestDispatcher("user/detail.jsp").forward(request, response);
	}
}
