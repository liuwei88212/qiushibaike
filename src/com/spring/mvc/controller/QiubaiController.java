package com.spring.mvc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.data.domain.Page;

import com.spring.mvc.common.Const;
import com.spring.mvc.entity.Content;
import com.spring.mvc.service.QiubaiService;

public class QiubaiController extends BaseController {
	private static final long serialVersionUID = 1L;
	private static final String DEFAULT_ORDER = "article_time desc,funny_num desc";
	private QiubaiService qiubaiService;
    
    public void init() throws ServletException {
    	qiubaiService = new QiubaiService(); 
    }

    public void service(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {
    	System.out.println("================QiubaiController================");
    	String pageNumStr = request.getParameter("pageNum");
    	String from = request.getParameter("from");
    	Integer pageNum = (pageNumStr==null || "".equals(pageNumStr)) ? 1:Integer.parseInt(pageNumStr);
    	from = (from==null || "".equals(from)) ? Const.QB.menuMap.get("8hr"):from;
    	Page<Content> page = qiubaiService.selectByPage(pageNum, DEFAULT_NUMPERPAGE,DEFAULT_ORDER,from);
    	request.setAttribute("page", page);
    	request.setAttribute("from", from);
    	request.getRequestDispatcher("main2.jsp").forward(request, response);
    }
}
