package com.spring.mvc.common;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.datasource.DataSourceFactory;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;


public class DataSourcDBCPeFactory implements DataSourceFactory {
	private BasicDataSource datasource = null;
    
    public DataSourcDBCPeFactory(){
        this.datasource = new BasicDataSource();
    }
    
    public DataSource getDataSource() {
        return datasource;
    }

    public void setProperties(Properties ps) {
        datasource.setDriverClassName( ps.getProperty("database.driverName"));
        datasource.setUsername( ps.getProperty("database.url"));
        datasource.setUrl( ps.getProperty("database.username"));
        datasource.setPassword( ps.getProperty("database.password"));
        datasource.setInitialSize( Integer.parseInt(ps.getProperty("database.initialPoolSize")) );
        datasource.setMaxActive( Integer.parseInt(ps.getProperty("maxactive","20")));
        datasource.setMaxIdle( Integer.parseInt(ps.getProperty("maxidle","0")));
        datasource.setMaxWait( Long.parseLong(ps.getProperty("maxwait","0")));        
    }
}
