package com.spring.mvc.common;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Const {
	public static void main(String[] args) {
		Map<String,String> menuMap = Const.QB.menuMap;
		for(Iterator<Entry<String, String>> it = menuMap.entrySet().iterator();it.hasNext();){
			Map.Entry<String, String> m=(Map.Entry<String, String>)it.next();
			System.out.println( m.getKey() + ":" + m.getValue());
		}
	}
	public static class QB{
//		public static final String imgRoot = "D:/qiubai/images";
		public static final String imgRoot = "/home/bae/app/qiubai/images";
		public static final int MAX_PAGE = 3; //采集分页数
		public static final int MAX_NEWS = 50; //审帖数量
		public static Map<String,String> urlMap = new HashMap<String, String>();
		public static Map<String,String> menuMap = new HashMap<String, String>();
		static{
			urlMap.put("new", "http://webinsp.qiushibaike.com/new3/fetch?t=");
			urlMap.put("www", "http://www.qiushibaike.com/");
			urlMap.put("wap3", "http://wap3.qiushibaike.com/");
			
			
			menuMap.put("8hr", "8hr");//热门
			menuMap.put("hot", "hot");//24小时
			menuMap.put("week", "week");//7天
			menuMap.put("month", "month");//30天
			menuMap.put("imgrank", "imgrank");// 硬菜
			menuMap.put("pic", "pic");// 时令
			menuMap.put("late", "late");// 最新
		}
		
		public String getFrom(String from){
			return menuMap.get(from);
		}
	}
	
	public static String[] UserAgent = {
		"Mozilla/5.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.2",
		"Mozilla/5.0 (iPad; U; CPU OS 3_2_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B500 Safari/531.21.11",
		"Mozilla/5.0 (SymbianOS/9.4; Series60/5.0 NokiaN97-1/20.0.019; Profile/MIDP-2.1 Configuration/CLDC-1.1) AppleWebKit/525 (KHTML, like Gecko) BrowserNG/7.1.18121",
		"Nokia5700AP23.01/SymbianOS/9.1 Series60/3.0",
		"UCWEB7.0.2.37/28/998", "NOKIA5700/UCWEB7.0.2.37/28/977",
		"Openwave/UCWEB7.0.2.37/28/978",
		"Mozilla/4.0 (compatible; MSIE 6.0; ) Opera/UCWEB7.0.2.37/28/989" };
}
