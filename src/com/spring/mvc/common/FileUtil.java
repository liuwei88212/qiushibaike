package com.spring.mvc.common;

import java.io.File;

public class FileUtil {
	public static boolean createFile(String path) {
		File file = new File(path);
		try {
			if (file != null) {
				File parent = file.getParentFile();
				if (parent != null && !parent.exists()) {
					parent.mkdirs();// 创建父级目录
				}
				return file.createNewFile();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean createFile(File file) {
		try {
			if (file != null) {
				File parent = file.getParentFile();
				if (parent != null && !parent.exists()) {
					parent.mkdirs();// 创建父级目录
				}
				if(file.exists()){
					file.delete();
				}
				return file.createNewFile();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
