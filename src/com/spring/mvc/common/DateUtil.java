package com.spring.mvc.common;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 日期工具类
 * 
 * @author Administrator
 * 
 */
public class DateUtil {

	private static SimpleDateFormat sdf = null;

	public static boolean isYYYY_MM_DD(String dateString){
		String reg1 = "(\\d{4}-(\\d{2}|\\d{1})-(\\d{2}|\\d{1}))";// yyyy-MM-dd
		Pattern pat1 = Pattern.compile(reg1);
		Matcher mat1 = pat1.matcher(dateString);
		return mat1.find();
	}
	public static Long parseDateStrToLong(String dateString){
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return formatDateToLong(sdf.parse(dateString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static Long parseDateStringToLong(String dateString) {
		Long result = null;
		try {
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			SimpleDateFormat sdf3 = new SimpleDateFormat("yyyyMMddHHmmss");
			String reg1 = "(\\d{4}-(\\d{2}|\\d{1})-(\\d{2}|\\d{1}))\\s(\\d{2}|\\d{1}):(\\d{2}|\\d{1}):(\\d{2}|\\d{1})";// yyyy-MM-dd
																														// HH:mm:ss
			String reg2 = "(\\d{4}/(\\d{2}|\\d{1})/(\\d{2}|\\d{1}))\\s((\\d{1}|\\d{2}):(\\d{1}|\\d{2}):(\\d{1}|\\d{2}))"; // yyyy/MM/dd
																															// HH:mm:ss
			String reg3 = "\\d{14}"; // yyyyMMddHHmmss;

			// ===============================================
			Pattern pat1 = Pattern.compile(reg1);
			Matcher mat1 = pat1.matcher(dateString);
			if (mat1.find()) {
				// System.out.println("reg1= "+mat1.group(0));
				Date d1 = sdf1.parse(dateString);
				result = formatDateToLong(d1);
				// System.out.println(""+cal.get(Calendar.YEAR)+formatDate(cal.get(Calendar.MONTH)+1)+formatDate(cal.get(Calendar.DAY_OF_MONTH)));
			}
			// ===============================================
			Pattern pat2 = Pattern.compile(reg2);
			Matcher mat2 = pat2.matcher(dateString);
			if (mat2.find()) {
				System.out.println("reg2= " + mat2.group(0));
				Date d2 = sdf2.parse(dateString);
				result = formatDateToLong(d2);
			}
			// ===============================================
			Pattern pat3 = Pattern.compile(reg3);
			Matcher mat3 = pat3.matcher(dateString);
			if (mat3.find()) {
				System.out.println("reg3= " + mat3.group(0));
				Date d3 = sdf3.parse(dateString);
				result = formatDateToLong(d3);
			}
		} catch (Exception e) {
			System.out.println("--------------日期格式不正确----------------");
		}
		// System.out.println(result);
		return result;
	}

	private static String formatDate(int number) {
		return new DecimalFormat("00").format(number);
	}

	// 将yyyy-MM-dd HH:mm:ss 格式的字符串解析成Date类型
	public static Date parseStringToDate(String dateString, String pattern) {
		try {
			if (dateString != null && !"".equals(dateString)) {
				sdf = new SimpleDateFormat(pattern);
				return sdf.parse(dateString);
			}
		} catch (Exception e) {
			return null;
			// e.printStackTrace();
		}
		return null;
	}

	// 将Date类型转换成String
	public static String formatDateToString(Date date, String pattern) {
		if (date != null) {
			sdf = new SimpleDateFormat(pattern);
			return sdf.format(date);
		}
		return "";
	}

	public static Long formatDateToLong(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		StringBuffer sb = new StringBuffer();
		sb.append(cal.get(Calendar.YEAR));
		sb.append(formatDate(cal.get(Calendar.MONTH) + 1));
		sb.append(formatDate(cal.get(Calendar.DAY_OF_MONTH)));
		sb.append(formatDate(cal.get(Calendar.HOUR_OF_DAY)));
		sb.append(formatDate(cal.get(Calendar.MINUTE)));
		sb.append(formatDate(cal.get(Calendar.SECOND)));
		// String s =
		// ""+cal.get(Calendar.YEAR)+formatDate(cal.get(Calendar.MONTH)+1)+formatDate(cal.get(Calendar.DAY_OF_MONTH));
		return "".equals(sb.toString()) ? null : Long.parseLong(sb.toString());
	}

	public static Long formatDateToLong(Calendar cal) {
		StringBuffer sb = new StringBuffer();
		sb.append(cal.get(Calendar.YEAR));
		sb.append(formatDate(cal.get(Calendar.MONTH) + 1));
		sb.append(formatDate(cal.get(Calendar.DAY_OF_MONTH)));
		sb.append(formatDate(cal.get(Calendar.HOUR_OF_DAY)));
		sb.append(formatDate(cal.get(Calendar.MINUTE)));
		sb.append(formatDate(cal.get(Calendar.SECOND)));
		// String s =
		// ""+cal.get(Calendar.YEAR)+formatDate(cal.get(Calendar.MONTH)+1)+formatDate(cal.get(Calendar.DAY_OF_MONTH));
		return "".equals(sb.toString()) ? null : Long.parseLong(sb.toString());
	}

	/**
	 * 根据用户输入时间格式格式化用户输入的时间
	 * 
	 * @param date
	 *            需要转换的时间
	 * @param pattern
	 *            需要转换的格式 eg:"yyyy-MM-dd HH:mm:ss"
	 * @return
	 */
	public static String getDateString(Date date, String pattern) {
		try {
			SimpleDateFormat sdf = null;
			if (null == pattern || "".equals(pattern.trim())) {
				sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			} else {
				sdf = new SimpleDateFormat(pattern);
			}
			return sdf.format(date);
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * 把用户输入的String类型的时间转换成Date
	 * 
	 * @param dateString
	 *            时间字符串
	 * @param pattern
	 *            制定时间字符串格式 eg:"yyyy-MM-dd HH:mm:ss"
	 * @return
	 */
	public static Date getDate(String dateString, String pattern) {
		try {
			SimpleDateFormat sdf = null;
			if (null == pattern || "".equals(pattern.trim())) {
				sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			} else {
				sdf = new SimpleDateFormat(pattern);
			}
			return sdf.parse(dateString);
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * 将日期转化为Long型 格式为"yyyyMMddHHmmss"
	 * 
	 * @param date
	 * @return String date
	 */
	public static Long getLongDate(Date date) {
		Long time = null;
		String datetime = null;
		SimpleDateFormat dformate = new SimpleDateFormat("yyyyMMddHHmmss");
		datetime = dformate.format(date);
		if (datetime != null) {
			time = Long.parseLong(datetime);
		}
		return time;
	}

	/**
	 * 将当前时间转化为Long型 格式为"yyyyMMddHHmmss"
	 * 
	 * @param date
	 * @return String date
	 */
	public static Long getLongDate() {
		return getLongDate(new Date());
	}

	/**
	 * 把Long型的"yyyyMMddHHmmss"时间格式转换成Date对象
	 * 
	 * @param longTime
	 * @return
	 */
	public static Date getDateFromLong(Long longTime) {
		Date date = null;
		if (longTime != null) {
			date = getDate(longTime + "", "yyyyMMddHHmmss");
		}
		return date;
	}

	/**
	 * 从日期中获取年月 格式为yyyyMM 本月是哪个月
	 * 
	 * @param date
	 * @return
	 */
	public static Integer getYYYYMM(Date date) {
		if (date == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		return year * 100 + month;
	}

	/**
	 * 从日期中获取年月日 格式为yyyyMMdd 今天是哪一天
	 * 
	 * @param date
	 * @return
	 */
	public static Integer getYYYYMMDD(Date date) {
		if (date == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		return year * 10000 + month * 100 + day;
	}

	public static String formatLongToString(Long time) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = sdf.parse(time + "");
			return sdf2.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	// 求开始和结束时间的间隔是否大于一天
	public static boolean compareOneDay(String startTimeStr, String endTimeStr) {
		Date startTime = DateUtil.parseStringToDate(startTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Date endTime = DateUtil.parseStringToDate(endTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.setTime(startTime);
		c.add(Calendar.DAY_OF_MONTH, +1);// 开始时间的月份+1
		// 如果开始时间+1 大于或者等于结束时间，则 说明二者之间的差小于等于一天
		if (c.getTime().equals(endTime) || c.getTime().after(endTime)) {
			return false;
		} else {
			return true;
		}
	}

	// 求开始和结束时间的间隔是否大于一个月
	public static boolean compareTime(String startTimeStr, String endTimeStr) {
		Date startTime = DateUtil.parseStringToDate(startTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Date endTime = DateUtil.parseStringToDate(endTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.setTime(startTime);
		c.add(Calendar.MONTH, +1);// 开始时间的月份+1
		// 如果开始时间+1 大于或者等于结束时间，则 说明二者之间的差小于等于一个月
		if (c.getTime().equals(endTime) || c.getTime().after(endTime)) {
			return true;
		} else {
			return false;
		}
	}

	// 比较日期间隔是否大于3天
	public static boolean isLagerThreeDays(String startTimeStr,
			String endTimeStr) {
		Date startTime = DateUtil.parseStringToDate(startTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Date endTime = DateUtil.parseStringToDate(endTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.setTime(startTime);
		c.add(Calendar.DAY_OF_MONTH, 3);// 开始时间天数+3
		// 日期差小于3天
		if (c.getTime().equals(endTime) || c.getTime().after(endTime)) {
			return false;
		} else {
			return true;
		}
	}

	// 比较日期间隔是否大于7天
	public static boolean isLagerSevenDays(String startTimeStr,
			String endTimeStr) {
		Date startTime = DateUtil.parseStringToDate(startTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Date endTime = DateUtil.parseStringToDate(endTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.setTime(startTime);
		c.add(Calendar.DAY_OF_MONTH, +7);// 开始时间天数+6
		// 日期差小于7天
		if (c.getTime().equals(endTime) || c.getTime().after(endTime)) {
			return false;
		} else {
			return true;
		}
	}

	// 比较日期间隔是否大于14天
	public static boolean isLagerSeventeenDays(String startTimeStr,
			String endTimeStr) {
		Date startTime = DateUtil.parseStringToDate(startTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Date endTime = DateUtil.parseStringToDate(endTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.setTime(startTime);
		c.add(Calendar.DAY_OF_MONTH, +14);// 开始时间天数+6
		// 日期差小于7天
		if (c.getTime().equals(endTime) || c.getTime().after(endTime)) {
			return false;
		} else {
			return true;
		}
	}

	// 比较日期间隔是否大于12小时
	public static boolean isLagerTwelveHour(String startTimeStr,
			String endTimeStr) {
		Date startTime = DateUtil.parseStringToDate(startTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Date endTime = DateUtil.parseStringToDate(endTimeStr,
				"yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.setTime(startTime);
		c.add(Calendar.HOUR_OF_DAY, +12);// 开始时间小时+12
		// 日期差小于3天
		if (c.getTime().equals(endTime) || c.getTime().after(endTime)) {
			return false;
		} else {
			return true;
		}
	}

	// 是否是同一天
	public static boolean isLagerOneDay(String startTimeStr, String endTimeStr) {
		if (startTimeStr.substring(0, startTimeStr.indexOf(" ")).equals(
				endTimeStr.substring(0, endTimeStr.indexOf(" ")))) {
			return false;
		} else {
			return true;
		}
	}

	private static String dateFormat(String format, Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	public static String getMonthFirstDay() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH,
				calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		return dateFormat("yyyy-MM-dd", calendar.getTime());
	}
	
	public static void main(String[] args) {
		// DateUtil.parseStringToLong("2012-9-1 12:00:00");
		// DateUtil.parseStringToLong("2012/1/1 12:00:00");
		// DateUtil.parseStringToLong("20121212121212");
		// System.out.println(Calendar.getInstance().get(Calendar.MONTH));
		System.out.println(getMonthFirstDay());
	}
}
