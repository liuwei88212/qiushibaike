package com.spring.mvc.common;

import java.io.Serializable;

/**
 * 序列号对象：审核内容
 * @author Administrator
 *
 */
public class NewsJson implements Serializable{
	
	private static final long serialVersionUID = 5475074329179047179L;
	private Long user_id;
	private String img;
	private String tags;
	private Long ip;
	private String content;
//	private String similar;
	private Integer id;
	
	
	
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	public Long getIp() {
		return ip;
	}
	public void setIp(Long ip) {
		this.ip = ip;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
