package com.spring.mvc.listener;

import java.util.Calendar;
import java.util.TimerTask;
import javax.servlet.ServletContext;
import com.spring.mvc.thread.ThreadUtil;

public class QiubaiTask extends TimerTask {
	private static final int SCHEDULE_HOUR = 0;
	private static boolean isRunning = false;
	private ServletContext context = null;
	
	public QiubaiTask(ServletContext context) {
		this.context = context;
	}

	public void run() {
		Calendar cal = Calendar.getInstance();
		// System.out.println(isRunning);
		if (!isRunning) {
//			if (SCHEDULE_HOUR == cal.get(Calendar.MINUTE)){}
			isRunning = true;
			context.log("================>开始执行指定任务<================");

			//执行任务
			executeTask();

			// 指定任务执行结束
			isRunning = false;
			context.log("================>指定任务执行结束<================");
		} else {
			context.log("上一次任务执行还未结束");
		}
	}

	/**
	 * 执行任务
	 */
	public void executeTask() {
		//qiubaiService.loadWWW();
		//newsService.load();
		ThreadUtil.loadContent("www");//内容
		ThreadUtil.loadNews();//审帖
	}
}
