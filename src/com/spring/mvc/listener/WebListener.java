package com.spring.mvc.listener;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class WebListener implements ServletContextListener{
	
	private Timer timer = null;
	private int startTime = 10;//单位分钟
	private int intervalTime = 30;//分钟

	public void contextInitialized(ServletContextEvent event) {
		timer = new java.util.Timer(true);
        event.getServletContext().log("================<定时器已启动>================"); 
        timer.schedule(new QiubaiTask(event.getServletContext()), startTime*60*1000, intervalTime*60*1000);//每隔10分钟
        event.getServletContext().log("================<已经添加任务调度表>: "+ startTime+"分钟后执行，间隔周期"+intervalTime+"分钟");
	}

	public void contextDestroyed(ServletContextEvent event) {
		 timer.cancel();
	     event.getServletContext().log("================>定时器销毁");
	}
}
