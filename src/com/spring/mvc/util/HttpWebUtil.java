package com.spring.mvc.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.spring.mvc.common.Const;
import com.spring.mvc.common.DateUtil;
import com.spring.mvc.common.FileUtil;

/**
 * 使用java自带的URL分析HTML 工具类 
 */
public class HttpWebUtil {
	
	private final static String default_charset = "UTF-8";
	private final static int timeout = 60000; 
	
	public static void main(String[] args) throws Exception {
//		String content = getWebContent("http://www.qiushibaike.com/",default_charset);
//		System.out.println(content);
		
//		Document doc = getDoc("http://www.qiushibaike.com/");
		
		//审核：http://webinsp.qiushibaike.com/inspect
//		Document doc = getDoc("http://webinsp.qiushibaike.com/new3/fetch?t="+Math.floor(Math.random()*5000 + 10000));
		
		
		String content = getJsonFromUrl("http://webinsp.qiushibaike.com/new3/fetch?t=12336",default_charset);
		System.out.println(content);
	}
	
	public static Document getDoc(String urlString,String charset){
		Document doc = null;
		try {
			String html = getWebContent(urlString,charset);
//			System.out.println(html);
			doc = Jsoup.parse(html);
			return doc ;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doc;
	}
	
	public static void getImages(String url,String charset) {
		Document doc = getDoc(url,charset);
		Elements articles = doc.select("div#content-left").select("div.article");
		for(Element article:articles){
			if(article.select("div").hasClass("thumb")){
				String title = article.select("div.content").attr("title");
				String artileDate = (DateUtil.parseDateStringToLong(title)+"").substring(0, 8);
				
				String imgSrc = article.select(".thumb").select("img").first().attr("src");
				String fileName = imgSrc.substring(imgSrc.lastIndexOf("/")+1);
				downloadImg(imgSrc,fileName,artileDate);
			}
		}
	}
	
	public static void downloadImg(String src,String fileName,String artileDate) {
		try {
			System.out.println("==>download image: "+src);
			URL url = new URL(src);
			URLConnection uc = url.openConnection();
			InputStream is = uc.getInputStream();
			artileDate = artileDate==null?DateUtil.formatDateToString(new Date(), "yyyyMMdd"):artileDate;
			String imgPath = Const.QB.imgRoot+"/"+artileDate+"/"+fileName;
			//System.out.println("save in:"+imgPath);
			File file = new File(imgPath);
			FileUtil.createFile(file);
			FileOutputStream out = new FileOutputStream(file);
			int i = 0;
			while ((i = is.read()) != -1) {
				out.write(i);
			}
			is.close();
			//System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getJsonFromUrl(String urlString,String charset) throws Exception {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("accept", "*/*");
		conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
		conn.setRequestProperty("Accept-Language","	zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
		conn.setRequestProperty("Cookie", "__utma=210674965.1401285799.1406462071.1416642350.1416746785.23; __utmz=210674965.1416642350.22.6.utmcsr=baidu|utmccn=(organic)|utmcmd=organic|utmctr=%E7%B3%97%E4%BA%8B%E7%99%BE%E7%A7%91; Hm_lvt_2670efbdd59c7e3ed3749b458cafaa37=1415031917,1416642349,1416746785,1416746818; _qqq_session_id=aa2e92df88c9ec7595519fce7e45cc99c4eac992; _qqq_uuid_=6df2c1077aeebe3b2ca5f0b52f570d76b8bf2a9c; Hm_lpvt_2670efbdd59c7e3ed3749b458cafaa37=1416746871; __utmc=210674965; _qqq_session=fe1c6f176efc4b1e53f676d3d7559f2dcd901c06; _qqq_user=6L2s6KeS6YGH6KeB5L2g5aa5; _qqq_user_id=7790542; BAIDU_DUP_lcr=http://openapi.qzone.qq.com/oauth/show?which=Login&display=pc&client_id=100251437&response_type=code&redirect_uri=www.qiushibaike.com/session?src=qq; _qqq_auth_token=fe1c6f176efc4b1e53f676d3d7559f2dcd901c06Host	webinsp.qiushibaike.com"); 
		conn.setRequestProperty("user-agent","Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
		conn.connect();
		
		InputStream is = conn.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line = "";
		StringBuffer buff = new StringBuffer();
		while ((line = reader.readLine()) != null) {
            System.out.println(line);
            buff.append(line);
        }
		reader.close();
		conn.disconnect();
		return "";
	}
	
	
	public static String getWebContent(String urlString,String charset) throws Exception {
		if (urlString == null || urlString.length() == 0) {
			return "";
		}
		urlString = (urlString.startsWith("http://") || urlString
				.startsWith("https://")) ? urlString : ("http://" + urlString)
				.intern();
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		
		conn.setRequestProperty("accept", "*/*");
		conn.setRequestProperty("Pragma", "no-cache");
		conn.setRequestProperty("Cache-Control", "no-cache");
		conn.setConnectTimeout(timeout);
		
		conn.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
		
//		int temp = Integer.parseInt(Math.round(Math.random()* (UserAgent.length - 1))+ "");
//		conn.setRequestProperty("User-Agent", UserAgent[temp]); // 模拟手机系统
//		conn.setRequestProperty("Accept",
//				"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");// 只接受text/html类型，当然也可以接受图片,pdf,*/*任意，就是tomcat/conf/web里面定义那些
		
		try {
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				System.out.println(conn.getResponseCode());
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		InputStream input = conn.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(input,charset));
		String line = null;
		StringBuffer sb = new StringBuffer("");
		while ((line = reader.readLine()) != null) {
			sb.append(line+"\n");
		}
		if (reader != null) {
			reader.close();
		}
		if (conn != null) {
			conn.disconnect();
		}
		return sb.toString();
	}

	public static String[] UserAgent = {
			"Mozilla/5.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.2",
			"Mozilla/5.0 (iPad; U; CPU OS 3_2_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B500 Safari/531.21.11",
			"Mozilla/5.0 (SymbianOS/9.4; Series60/5.0 NokiaN97-1/20.0.019; Profile/MIDP-2.1 Configuration/CLDC-1.1) AppleWebKit/525 (KHTML, like Gecko) BrowserNG/7.1.18121",
			"Nokia5700AP23.01/SymbianOS/9.1 Series60/3.0",
			"UCWEB7.0.2.37/28/998", "NOKIA5700/UCWEB7.0.2.37/28/977",
			"Openwave/UCWEB7.0.2.37/28/978",
			"Mozilla/4.0 (compatible; MSIE 6.0; ) Opera/UCWEB7.0.2.37/28/989" };
}
