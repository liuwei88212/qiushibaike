package com.spring.mvc.util;

import java.io.Reader;
import java.sql.SQLException;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class IBatisUtils {

	private static SqlSessionFactory sqlSessionFactoryQiubai;

	static {
		try {
			String resource = "mybatis-config-qiubai.xml";
			Reader reader = Resources.getResourceAsReader(resource);
			sqlSessionFactoryQiubai = new SqlSessionFactoryBuilder().build(reader);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static SqlSessionFactory getSqlSessionFactoryQiubai() {
		return sqlSessionFactoryQiubai;
	}

	
	public static <T, P> T execInQiubai(Class<P> p, DbCallback<T, P> callback) {
		SqlSession session = null;
		try {
			session = IBatisUtils.getSqlSessionFactoryQiubai().openSession();
			P obj = session.getMapper(p);
			return callback.doInDb(obj);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	public static interface DbCallback<T, P> {
		T doInDb(P p);
	}

	public static void main(String[] args) throws SQLException {
		SqlSession session = getSqlSessionFactoryQiubai().openSession();
		System.out.println(session.getConnection());
	}
}
