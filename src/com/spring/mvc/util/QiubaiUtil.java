package com.spring.mvc.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

/**
 *糗百登录操作
 */
public class QiubaiUtil {
	private static String loginUrl = "http://www.qiushibaike.com/session.js";
	private static String login = "转角遇见你妹";
	private static String password = "5989283";
	private static String duration = "4899";
	private static String remember_me = "checked";
	private static CloseableHttpClient client = HttpClients.createDefault();
	static{
		try {
			login();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		System.out.println(getContent("http://webinsp.qiushibaike.com/new3/fetch?t=14215"));
	}
	
	public static void login() throws ClientProtocolException, IOException{
		HttpPost method = new HttpPost(loginUrl);  
		// 设置登陆时要求的信息，用户名和密码
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("login",login));
		nvps.add(new BasicNameValuePair("password",password));
		nvps.add(new BasicNameValuePair("duration",duration));
		nvps.add(new BasicNameValuePair("remember_me",remember_me));
		method.setEntity(new UrlEncodedFormEntity(nvps,HTTP.UTF_8));
		
		// 使用POST方法
		CloseableHttpResponse response = client.execute(method);

		// 打印服务器返回的状态
		System.out.println(response.getStatusLine());
		// 打印返回的信息
		System.out.println(EntityUtils.toString(response.getEntity(),"UTF-8"));
		// 释放连接
		method.releaseConnection();
	}
	
	public static String getContent(String url){
		try {
			HttpGet method = new HttpGet(url);
			method.setHeader("accept", "*/*");
			method.setHeader("Pragma", "no-cache");
			method.setHeader("Cache-Control", "no-cache");
			method.setHeader("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			
			// 使用POST方法
			// HttpMethod method = new PostMethod("http://java.sun.com");
			CloseableHttpResponse response = client.execute(method);
	
			// 打印服务器返回的状态
			System.out.println(response.getStatusLine());
			// 打印返回的信息
			String content = EntityUtils.toString(response.getEntity());
			// 释放连接
			method.releaseConnection();
			return content;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
