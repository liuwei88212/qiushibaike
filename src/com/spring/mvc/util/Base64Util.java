package com.spring.mvc.util;

import java.io.Reader;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import org.apache.ibatis.io.Resources;
import sun.org.mozilla.javascript.internal.NativeObject;

/**
 * 此加密是根据JS加密算法转换而来
 * @author Administrator
 *
 */
public class Base64Util {
	private static ScriptEngine engine = null; 
	private static Invocable invocableEngine = null;
	
	public static void main(String[] args) {
//		String encodeStr = encodeUTF8("刘维");
//		decodeUTF8(encodeStr);
		//http://pic.qiushibaike.com/system/pictures/8086/80865563/medium/app80865563.jpg
		//http://pic.qiushibaike.com/system/pictures/8086/80865571/medium/app80865571.jpg
		decode("YXBwODA4NjU1NzMuanBn\n");
	}
	
	static {
		ScriptEngineManager manager = new ScriptEngineManager();
		engine = manager.getEngineByName("JavaScript");
		try {
			String resource = "js/Base64.js";
			Reader reader = Resources.getResourceAsReader(resource);
			// 开始执行Base64.js里的程序
			engine.eval(reader);
			
			// 转换为Invocable
			invocableEngine = (Invocable) engine;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static String encode(String str){
		if(str==null || "".equals(str)){
			return null;
		}
		
		NativeObject nativeObj = (NativeObject) engine.get("Base64");
		String encodeStr = null;
		try {
			encodeStr = (String) invocableEngine.invokeMethod(nativeObj, "encode", str);
			System.out.println("加密："+str+"->"+encodeStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encodeStr; 
	}
	
	public static String decode(String encodeStr){
		if(encodeStr==null || "".equals(encodeStr)){
			return null;
		}
		NativeObject nativeObj = (NativeObject) engine.get("Base64");
		String str = null;
		try {
			System.out.println("密文："+ encodeStr);
			str = (String) invocableEngine.invokeMethod(nativeObj, "decode", encodeStr);
			System.out.println("解码后："+ str);
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str; 
	}
	public static String decodeImg(String encodeStr){
		if(encodeStr==null || "".equals(encodeStr)){
			return null;
		}
		NativeObject nativeObj = (NativeObject) engine.get("Base64");
		String str = null;
		try {
			System.out.println("密文："+ encodeStr);
			str = (String) invocableEngine.invokeMethod(nativeObj, "decodeImg", encodeStr);
			System.out.println("解码后："+encodeStr+"->"+str);
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str; 
	}
	
	public static String decodeUTF8(String encodeStr){
		NativeObject nativeObj = (NativeObject) engine.get("Base64");
		String str = null;
		try {
			System.out.println("密文："+ encodeStr);
			str = (String) invocableEngine.invokeMethod(nativeObj, "_utf8_decode", encodeStr);
			System.out.println("解码后："+ str);
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str; 
	}
	public static String encodeUTF8(String str){
		NativeObject nativeObj = (NativeObject) engine.get("Base64");
		String encodeStr = null;
		try {
			encodeStr = (String) invocableEngine.invokeMethod(nativeObj, "_utf8_encode", str);
			System.out.println("加密："+str+"->"+encodeStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encodeStr; 
	}
}
