package com.spring.mvc.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.spring.mvc.common.Const;
import com.spring.mvc.common.FileUtil;

/**
 * JSOUP 分析HTML工具
 * @author Administrator
 *
 */
public class JsoupWebUtil {
	public static void main(String[] args) {
	}
	
	public static Connection getConnection(String url){
		Connection connection = Jsoup.connect(url);
		connection.userAgent(Const.UserAgent[Integer.parseInt(Math.round(Math.random()* (Const.UserAgent.length - 1))+ "")]);
		return connection;
	}
	public static Document getDoc(String url){
		Document doc = null;
		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
	}

	public static String getWebContent(String url) {
		Document doc = getDoc(url);
		Elements elements = doc.select("div.qiushi");
		StringBuffer sb = new StringBuffer();
		for (Element e : elements) {
			sb.append(e.html()+"\n");
		}
		return sb.toString();
	}
	
	public static void getImages(String url) {
		try {
			Document doc = Jsoup.connect(url).get();
			System.out.println(doc.title());
			Elements elements = doc.select("div.qiushi");
			for (Element e : elements) {
				for(Element a : e.select("a")){
					String aHref = a.attr("href");
					if(!aHref.endsWith(".jpg")){//用户头像
						
					}else{
						//上传图片
						String fileName = aHref.substring(aHref.lastIndexOf("/")+1);
						downloadImg(aHref, fileName);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void downloadImg(String src,String fileName) {
		try {
			System.out.println("==========>getImg: "+src);
			URL url = new URL(src);
			URLConnection uc = url.openConnection();
			InputStream is = uc.getInputStream();
			File file = new File("D:/qiubai/images/"+fileName);
			FileUtil.createFile(file);
			FileOutputStream out = new FileOutputStream(file);
			int i = 0;
			while ((i = is.read()) != -1) {
				out.write(i);
			}
			is.close();
			System.out.println("->下载完成");
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
