package com.spring.mvc.thread;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.spring.mvc.common.Const;
import com.spring.mvc.entity.Content;
import com.spring.mvc.service.QiubaiService;
import com.spring.mvc.util.HttpWebUtil;
import com.spring.mvc.util.JsoupWebUtil;

public class ContentThread implements Runnable{
	
	private static final String charset = "utf-8";
	private String url;
	private String from;
	private String urlType;
	private QiubaiService contentService;
	
	public ContentThread(QiubaiService contentService,String url,String from,String urlType){
		this.contentService = contentService;
		this.url = url;
		this.from = from;
		this.urlType = urlType;
	}
	
	public void run() {
		ExecutorService pool = Executors.newFixedThreadPool(Const.QB.MAX_PAGE);
		for(int i=1; i<= Const.QB.MAX_PAGE;i++){
			final String newUrl = url +"/page/"+i;
			pool.execute(new Thread(){
				public void run() {
					System.out.println("===>采集页面url: " + newUrl);
					if("www".equals(urlType)){
						List<Content> list = contentService.getListFromWWW(newUrl,from);
						contentService.insertBatch(list);
						//HttpWebUtil.getImages(newUrl,charset);
					}else if("wap3".equals(urlType)){
						List<Content> list = contentService.getListFromWap3(newUrl);
						contentService.insertBatch(list);
						//JsoupWebUtil.getImages(newUrl);//下载图片
					}
				}
			});
		}
	}
}
