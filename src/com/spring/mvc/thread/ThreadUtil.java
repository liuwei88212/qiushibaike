package com.spring.mvc.thread;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.spring.mvc.common.Const;
import com.spring.mvc.service.NewsService;
import com.spring.mvc.service.QiubaiService;

public class ThreadUtil {
	
	public static NewsService newsService = new NewsService();
	private static QiubaiService contentService = new QiubaiService();
	
	public static void main(String[] args) {
		loadContent("www");
	}
	
	public static void loadContent(String urlType){
		String rootUrl =  Const.QB.urlMap.get(urlType);
		Map<String,String> menuMap = Const.QB.menuMap;
//		ExecutorService pool = Executors.newFixedThreadPool(10);
		ThreadPoolExecutor threadPool = new ThreadPoolExecutor(5, 5, 3,
		        TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100),
		        new ThreadPoolExecutor.AbortPolicy());
		for(Iterator<Entry<String, String>> it = menuMap.entrySet().iterator();it.hasNext();){
			Map.Entry<String, String> m=(Map.Entry<String, String>)it.next();
			String from = m.getValue();
			String url = rootUrl + from;
			threadPool.execute(new ContentThread(contentService,url, from, urlType));
		}
		threadPool.shutdown();
		while(true){
			if(threadPool.isTerminated()){
				contentService.deleteDuplicate();
				break;
		    }
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void loadNews(){
//		ExecutorService pool = Executors.newFixedThreadPool(Const.QB.MAX_NEWS/10);
		ThreadPoolExecutor threadPool = new ThreadPoolExecutor(Const.QB.MAX_NEWS/10, Const.QB.MAX_NEWS/10, 3,
		        TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100),
		        new ThreadPoolExecutor.AbortPolicy());
		for(int i=1; i < Const.QB.MAX_NEWS;i++){
			threadPool.execute(new NewsThread(newsService));
		}
		threadPool.shutdown();
		while(true){
			if(threadPool.isTerminated()){
				newsService.deleteDuplicate();
				break;
		    }
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
