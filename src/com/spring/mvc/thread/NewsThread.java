package com.spring.mvc.thread;

import java.util.List;

import com.spring.mvc.entity.News;
import com.spring.mvc.service.NewsService;

public class NewsThread implements Runnable{
	
	private NewsService newsService ;
	
	public NewsThread(NewsService newsService){
		this.newsService = newsService;
	}
	
	public void run() {
		//System.out.println("==============采集审帖: "+Thread.currentThread().getName()+"==============");
		List<News> list = newsService.getNewsList();
		if(list!=null){
			newsService.insertBatch(list);	
		}
	}

}
