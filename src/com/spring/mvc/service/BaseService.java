package com.spring.mvc.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class BaseService {
	
	private static final int PAGESIZE = 50;
	
	public Pageable getPageable(int pageNum, int numPerPage) {
		pageNum = pageNum < 1 ? 0 : pageNum - 1;
		numPerPage = numPerPage < 1 ? PAGESIZE : numPerPage;
		Pageable pageable = new PageRequest(pageNum, numPerPage);
		return pageable;
	}
}
