package com.spring.mvc.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsoup.Jsoup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.alibaba.fastjson.JSON;
import com.spring.mvc.common.Const;
import com.spring.mvc.common.DateUtil;
import com.spring.mvc.common.NewsJson;
import com.spring.mvc.entity.News;
import com.spring.mvc.entity.NewsExample;
import com.spring.mvc.mapper.NewsMapper;
import com.spring.mvc.util.Base64Util;
import com.spring.mvc.util.HttpWebUtil;
import com.spring.mvc.util.IBatisUtils;
import com.spring.mvc.util.QiubaiUtil;

/**
 * 审核内容需要先登录才能获取
 * 
 * @author Administrator
 *
 */
public class NewsService extends BaseService{
	
	private static final String charset = "utf-8";
	
	public static void main(String[] args) {
		NewsService service = new NewsService();
		for(int i=0;i<10;i++){
			service.insertBatch(service.getNewsList());
		}
		service.deleteDuplicate();
	}
	
	public void load(){
		for(int i=1; i<Const.QB.MAX_PAGE*10;i++){
			insertBatch(getNewsList());
		}
		deleteDuplicate();
	}
	
	public List<News> getNewsList(){
		List<NewsJson> jsonNews = getNews();
		List<News> newList = null;
		if(jsonNews!=null){
			newList = new ArrayList<News>();
			for(NewsJson json:jsonNews){
				News record = new News();
				record.setContent(json.getContent());
				record.setImg(json.getImg());
				record.setUserId(json.getUser_id()+"");
				record.setTags(json.getTags());
				record.setId(json.getId());
				newList.add(record);
				
				if(json.getImg()!=null && !"".equals(json.getImg())){
					String imgSrc = Jsoup.parse(json.getImg()).select("img").attr("src");
					String fileName = imgSrc.substring(imgSrc.lastIndexOf("/")+1);
					String artileDate = DateUtil.formatDateToString(new Date(), "yyyyMMdd");
					HttpWebUtil.downloadImg(imgSrc, fileName, artileDate);
				}
				
			}
		}
		return newList;
	}
	public List<NewsJson> getNews(){
		String newsUrl = Const.QB.urlMap.get("new")+Math.floor(Math.random()*5000 + 10000);
		System.out.println("===>采集审帖Url: "+newsUrl);
		try {
			List<NewsJson> list = null;
			String content = QiubaiUtil.getContent(newsUrl);
			if(content!=null && !"".equals(content)){
				list = JSON.parseArray(content,NewsJson.class);//JSON转换List
				for(NewsJson n:list){//解密
					n.setContent(Base64Util.decode(n.getContent()));
					n.setImg(Base64Util.decodeImg(n.getImg()));
					n.setTags(Base64Util.decode(n.getTags()));
				}
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void insertBatch(List<News> list) {
		if(list==null || list.size()==0){
			return ;
		}
		SqlSession session = null;
		try {
			session = IBatisUtils.getSqlSessionFactoryQiubai().openSession();
			NewsMapper mapper = session.getMapper(NewsMapper.class);
			mapper.insertBatch(list);
			session.commit();
			System.out.println("===>插入数据库News: "+list.size()+"条数据");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public Page<News> selectByPage(int pageNum, int numPerPage, String order) {
		SqlSession session = null;
		try {
			session = IBatisUtils.getSqlSessionFactoryQiubai().openSession();
			NewsMapper mapper = session.getMapper(NewsMapper.class);
			
			NewsExample example = new NewsExample();
			Pageable pageable = getPageable(pageNum, numPerPage);
			example.setOffset(pageable.getOffset());
			example.setPagesize(pageable.getPageSize());
			example.setOrderByClause(order);
			example.createCriteria().andImgIsNotNull();
			
			List<News> list = mapper.selectByExample(example);
			int count = mapper.countByExample(example);
			Page<News> page = new PageImpl<News>(list, pageable, count);
			return page;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return null;
	}
	
	public void deleteDuplicate() {
		SqlSession session = null;
		try {
			session = IBatisUtils.getSqlSessionFactoryQiubai().openSession();
			NewsMapper mapper = session.getMapper(NewsMapper.class);
			int result = mapper.deleteDuplicate();
			session.commit();
			System.out.println("===>删除重复数据 t_news :" + result);
			System.out.println("\n");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
}
