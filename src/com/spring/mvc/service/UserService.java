package com.spring.mvc.service;

import org.springframework.data.domain.Page;
import com.spring.mvc.entity.Content;
import com.spring.mvc.entity.User;

public class UserService extends BaseService{
	
	public QiubaiService qiubaiService = new QiubaiService();
	
	public User selectByUser(Long userSerial){
		Content content = qiubaiService.selectByUser(0, 1, userSerial).getContent().get(0);
		User user = new User();
		user.setUserId(content.getUserSerial());
		user.setUserName(content.getUserName());
		user.setUserHead(content.getUserHead());
		return user;
	}
	
	public Page<Content> selectByUser(int pageNum, int numPerPage, Long userId){
		return qiubaiService.selectByUser(pageNum, numPerPage, userId);
	}
}
