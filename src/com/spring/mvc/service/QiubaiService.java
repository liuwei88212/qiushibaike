package com.spring.mvc.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.ibatis.session.SqlSession;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.spring.mvc.common.Const;
import com.spring.mvc.common.DateUtil;
import com.spring.mvc.entity.Content;
import com.spring.mvc.entity.ContentExample;
import com.spring.mvc.mapper.ContentMapper;
import com.spring.mvc.util.HttpWebUtil;
import com.spring.mvc.util.IBatisUtils;
import com.spring.mvc.util.JsoupWebUtil;


public class QiubaiService extends BaseService{
	
	private static final String charset = "utf-8";
	
	public static void main(String[] args) {
		QiubaiService service = new QiubaiService();
//		for(Content c:list){
//			System.out.println(c.getUserSerial()+","+c.getUserHead()+","+c.getUserName()+","+c.getContentMain()+","+c.getContentImg());
//			System.out.println(c.getFunnyNum()+","+c.getNotfunnyNum()+","+c.getCommentNum());
//		}
		
		System.out.println("===================采集=======================\n\n");
//		String wap3 =  Const.QB.urlMap.get("wap3");
//		service.getContent(wap3,"wap3");
		
		String www =  Const.QB.urlMap.get("www");
		service.getContent(www,"www");
	}
	
	public  void getContent(String rootUrl,String urlType){
		deleteDuplicate();
		Map<String,String> menuMap = Const.QB.menuMap;
		for(Iterator<Entry<String, String>> it = menuMap.entrySet().iterator();it.hasNext();){
			Map.Entry<String, String> m=(Map.Entry<String, String>)it.next();
			String from = m.getValue();
			String url = rootUrl + from;
			
			for(int i=1; i<= Const.QB.MAX_PAGE;i++){
				String newUrl = url +"/page/"+i;
				if("www".equals(urlType)){
					List<Content> list = getListFromWWW(newUrl,from);
					insertBatch(list);
				}else if("wap3".equals(urlType)){
					List<Content> list = getListFromWap3(newUrl);
					insertBatch(list);
				}
			}
		}
		deleteDuplicate();
	}
	
	public void loadWAP3(){
		System.out.println("===================采集开始WAP3=======================\n\n");
		String wap3 = Const.QB.urlMap.get("wap3"); //8小时最糗
		getContent(wap3,"wap3");
	}
	
	public void loadWWW(){
		System.out.println("===================采集开始WWW=======================\n\n");
		String www =  Const.QB.urlMap.get("www");
		getContent(www,"www");
	}
	
	private void getImages(Document doc) {
		Elements imgElements = doc.getElementById("content").getElementsByTag("img");
		for(Element img:imgElements){
			String imgSrc = img.attr("src");
			String fileName = imgSrc.substring(imgSrc.lastIndexOf("/")+1);
			HttpWebUtil.downloadImg(imgSrc,fileName,null);
		}
	}
	
	public List<Content> getListFromWap3(String url){
//		System.out.println("===>get: "+url);
		Document doc = JsoupWebUtil.getDoc(url);
		getImages(doc);//下载图片
		
		List<Content> list = new ArrayList<Content>();
		if(doc!=null){
			Elements elements = doc.select("div.qiushi");
			for(Element qiubai:elements){
				Content content = new Content();
				Element user = qiubai.select("p.user").first();
				if(user!=null){
					//用户信息
					String userHref = user.select("a").first().attr("href");
					String userImg = user.select("a").first().select("img").first().attr("src");
					String userName = user.select("a").first().ownText();
					content.setUserSerial(Long.parseLong(userHref.substring(userHref.lastIndexOf("/")+1)));
					content.setUserHead(userImg);
					content.setUserName(userName);
					content.setIsAnonymous(1);//非匿名
				}else{
					content.setIsAnonymous(0);//匿名
				}
				content.setContentMain(qiubai.ownText());
				for(Element a : qiubai.select("a")){
					String aHref = a.attr("href");
					if(aHref.endsWith(".jpg")){
						content.setContentImg(aHref);
					}
				}
				Element vote = qiubai.select("p.vote").first();
				for(int i=0;i< vote.select("a").size();i++){
					Element e = vote.select("a").get(i);
					if(i==0){
						content.setFunnyNum(Integer.parseInt(e.text()));
					}else if(i==1){
						content.setNotfunnyNum(Integer.parseInt(e.text()));
					}else{
						if(e.select("strong").size() != 0){
							content.setCommentNum(Integer.parseInt(e.select("strong").first().text()));
						}
					}
				}
				list.add(content);
			}
		}
		System.out.println("\n\n");
		return list;
	}
	
	public List<Content> getListFromWWW(String url,String from){
//		System.out.println("===>get: "+url);
		Document doc = HttpWebUtil.getDoc(url,charset);
		getImages(doc);//下载图片
		
		List<Content> list = new ArrayList<Content>();
		if(doc!=null){
			Elements articles = doc.select("div#content-left").select("div.article");
			for(Element article:articles){
				Content content = new Content();
				content.setArticleFrom(from);
				
				//文章ID
				String articleId = article.id().substring(article.id().lastIndexOf("_")+1);
				content.setArticleId(Long.parseLong(articleId));
				
				//用户信息
				Element user = article.select("div.author").first();
				if(user!=null){
					String userHref = user.select("a").first().attr("href");
					String userImg = user.select("a").first().select("img").first().attr("src");
					String userName = user.select("a").select("img").first().attr("alt");
					content.setUserSerial(Long.parseLong(userHref.substring(userHref.lastIndexOf("/")+1)));
					content.setUserHead(userImg);
					content.setUserName(userName);
					content.setIsAnonymous(1);//非匿名
				}else{
					content.setIsAnonymous(0);//匿名
				}
				
				//正文
				Element main = article.select("div.content").first();
				content.setContentMain(main.html());
				//时间
				content.setArticleTime(DateUtil.parseDateStrToLong(main.attr("title")));
				
				//图片
				if(article.select("div").hasClass("thumb")){
					Element img = article.select("div.thumb").select("img").first();
					content.setContentImg(img.attr("src"));
				}
				
				//赞
				Element stats = article.select("div.stats").first();
				for(Element span: stats.select("span")){
					if(span.hasClass("stats-vote")){
						Element vote = span.select(".stats-vote").first().select("i.number").first();
						content.setFunnyNum(Integer.parseInt(vote.text()));//赞
					}
					if(span.hasClass("stats-comments")){
						Element comments = span.select(".stats-comments").first();
						if(comments.hasClass("number")){
							String commentsNum = comments.select(".number").first().text();
							content.setCommentNum(Integer.parseInt(commentsNum));//回复
						}else{
							content.setCommentNum(0);
						}
					}
				}
				Elements down = article.select("div.stats-buttons").select("ul").select("li.comments");
				if(down.hasClass(("number"))){
					String downNum = down.select(".number").first().text();
					content.setNotfunnyNum(Integer.parseInt(downNum));
				}else{
					content.setNotfunnyNum(0);//囧
				}
				list.add(content);
			}
		}
		System.out.println("\n\n");
		return list;
	}
	
	public void insertBatch(List<Content> list) {
		System.out.println("===>插入数据库: "+list.size()+" 条记录");
		SqlSession session = null;
		try {
			session = IBatisUtils.getSqlSessionFactoryQiubai().openSession();
			ContentMapper mapper = session.getMapper(ContentMapper.class);
			mapper.insertBatch(list);
			session.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	
	public void deleteDuplicate() {
//		System.out.println("=========>delete Duplicate from Mysql ");
		SqlSession session = null;
		try {
			session = IBatisUtils.getSqlSessionFactoryQiubai().openSession();
			ContentMapper mapper = session.getMapper(ContentMapper.class);
			int result = mapper.deleteDuplicate();
			session.commit();
			System.out.println("===>删除重复记录 t_content :" + result);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public int count() {
		SqlSession session = null;
		try {
			session = IBatisUtils.getSqlSessionFactoryQiubai().openSession();
			ContentMapper mapper = session.getMapper(ContentMapper.class);
			return mapper.countByExample(new ContentExample());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return 0;
	}
	
	public Page<Content> selectByPage(int pageNum, int numPerPage, String order,String from) {
		SqlSession session = null;
		try {
			session = IBatisUtils.getSqlSessionFactoryQiubai().openSession();
			ContentMapper mapper = session.getMapper(ContentMapper.class);
			
			ContentExample example = new ContentExample();
			Pageable pageable = getPageable(pageNum, numPerPage);
			example.setOffset(pageable.getOffset());
			example.setPagesize(pageable.getPageSize());
			example.setOrderByClause(order);
			example.createCriteria().andArticleFromEqualTo(from);
			
			List<Content> list = mapper.selectByExample(example);
			int count = mapper.countByExample(example);
			Page<Content> page = new PageImpl<Content>(list, pageable, count);
			return page;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return null;
	}
	
	public Page<Content> selectByUser(int pageNum, int numPerPage, Long userId) {
		SqlSession session = null;
		try {
			session = IBatisUtils.getSqlSessionFactoryQiubai().openSession();
			ContentMapper mapper = session.getMapper(ContentMapper.class);
			
			ContentExample example = new ContentExample();
			Pageable pageable = getPageable(pageNum, numPerPage);
			example.setOffset(pageable.getOffset());
			example.setPagesize(pageable.getPageSize());
			example.createCriteria().andUserSerialEqualTo(userId);
			
			List<Content> list = mapper.selectByExample(example);
			int count = mapper.countByExample(example);
			Page<Content> page = new PageImpl<Content>(list, pageable, count);
			return page;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return null;
	}
	
}
