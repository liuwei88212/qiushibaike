package com.spring.mvc.entity;

import java.util.ArrayList;
import java.util.List;

public class ContentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int offset = -1;

    protected int pagesize = -1;

    public ContentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setOffset(int offset) {
        this.offset=offset;
    }

    public int getOffset() {
        return offset;
    }

    public void setPagesize(int pagesize) {
        this.pagesize=pagesize;
    }

    public int getPagesize() {
        return pagesize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andArticleIdIsNull() {
            addCriterion("article_id is null");
            return (Criteria) this;
        }

        public Criteria andArticleIdIsNotNull() {
            addCriterion("article_id is not null");
            return (Criteria) this;
        }

        public Criteria andArticleIdEqualTo(Long value) {
            addCriterion("article_id =", value, "articleId");
            return (Criteria) this;
        }

        public Criteria andArticleIdNotEqualTo(Long value) {
            addCriterion("article_id <>", value, "articleId");
            return (Criteria) this;
        }

        public Criteria andArticleIdGreaterThan(Long value) {
            addCriterion("article_id >", value, "articleId");
            return (Criteria) this;
        }

        public Criteria andArticleIdGreaterThanOrEqualTo(Long value) {
            addCriterion("article_id >=", value, "articleId");
            return (Criteria) this;
        }

        public Criteria andArticleIdLessThan(Long value) {
            addCriterion("article_id <", value, "articleId");
            return (Criteria) this;
        }

        public Criteria andArticleIdLessThanOrEqualTo(Long value) {
            addCriterion("article_id <=", value, "articleId");
            return (Criteria) this;
        }

        public Criteria andArticleIdIn(List<Long> values) {
            addCriterion("article_id in", values, "articleId");
            return (Criteria) this;
        }

        public Criteria andArticleIdNotIn(List<Long> values) {
            addCriterion("article_id not in", values, "articleId");
            return (Criteria) this;
        }

        public Criteria andArticleIdBetween(Long value1, Long value2) {
            addCriterion("article_id between", value1, value2, "articleId");
            return (Criteria) this;
        }

        public Criteria andArticleIdNotBetween(Long value1, Long value2) {
            addCriterion("article_id not between", value1, value2, "articleId");
            return (Criteria) this;
        }

        public Criteria andArticleTimeIsNull() {
            addCriterion("article_time is null");
            return (Criteria) this;
        }

        public Criteria andArticleTimeIsNotNull() {
            addCriterion("article_time is not null");
            return (Criteria) this;
        }

        public Criteria andArticleTimeEqualTo(Long value) {
            addCriterion("article_time =", value, "articleTime");
            return (Criteria) this;
        }

        public Criteria andArticleTimeNotEqualTo(Long value) {
            addCriterion("article_time <>", value, "articleTime");
            return (Criteria) this;
        }

        public Criteria andArticleTimeGreaterThan(Long value) {
            addCriterion("article_time >", value, "articleTime");
            return (Criteria) this;
        }

        public Criteria andArticleTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("article_time >=", value, "articleTime");
            return (Criteria) this;
        }

        public Criteria andArticleTimeLessThan(Long value) {
            addCriterion("article_time <", value, "articleTime");
            return (Criteria) this;
        }

        public Criteria andArticleTimeLessThanOrEqualTo(Long value) {
            addCriterion("article_time <=", value, "articleTime");
            return (Criteria) this;
        }

        public Criteria andArticleTimeIn(List<Long> values) {
            addCriterion("article_time in", values, "articleTime");
            return (Criteria) this;
        }

        public Criteria andArticleTimeNotIn(List<Long> values) {
            addCriterion("article_time not in", values, "articleTime");
            return (Criteria) this;
        }

        public Criteria andArticleTimeBetween(Long value1, Long value2) {
            addCriterion("article_time between", value1, value2, "articleTime");
            return (Criteria) this;
        }

        public Criteria andArticleTimeNotBetween(Long value1, Long value2) {
            addCriterion("article_time not between", value1, value2, "articleTime");
            return (Criteria) this;
        }

        public Criteria andArticleFromIsNull() {
            addCriterion("article_from is null");
            return (Criteria) this;
        }

        public Criteria andArticleFromIsNotNull() {
            addCriterion("article_from is not null");
            return (Criteria) this;
        }

        public Criteria andArticleFromEqualTo(String value) {
            addCriterion("article_from =", value, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andArticleFromNotEqualTo(String value) {
            addCriterion("article_from <>", value, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andArticleFromGreaterThan(String value) {
            addCriterion("article_from >", value, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andArticleFromGreaterThanOrEqualTo(String value) {
            addCriterion("article_from >=", value, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andArticleFromLessThan(String value) {
            addCriterion("article_from <", value, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andArticleFromLessThanOrEqualTo(String value) {
            addCriterion("article_from <=", value, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andArticleFromLike(String value) {
            addCriterion("article_from like", value, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andArticleFromNotLike(String value) {
            addCriterion("article_from not like", value, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andArticleFromIn(List<String> values) {
            addCriterion("article_from in", values, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andArticleFromNotIn(List<String> values) {
            addCriterion("article_from not in", values, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andArticleFromBetween(String value1, String value2) {
            addCriterion("article_from between", value1, value2, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andArticleFromNotBetween(String value1, String value2) {
            addCriterion("article_from not between", value1, value2, "articleFrom");
            return (Criteria) this;
        }

        public Criteria andUserSerialIsNull() {
            addCriterion("user_serial is null");
            return (Criteria) this;
        }

        public Criteria andUserSerialIsNotNull() {
            addCriterion("user_serial is not null");
            return (Criteria) this;
        }

        public Criteria andUserSerialEqualTo(Long value) {
            addCriterion("user_serial =", value, "userSerial");
            return (Criteria) this;
        }

        public Criteria andUserSerialNotEqualTo(Long value) {
            addCriterion("user_serial <>", value, "userSerial");
            return (Criteria) this;
        }

        public Criteria andUserSerialGreaterThan(Long value) {
            addCriterion("user_serial >", value, "userSerial");
            return (Criteria) this;
        }

        public Criteria andUserSerialGreaterThanOrEqualTo(Long value) {
            addCriterion("user_serial >=", value, "userSerial");
            return (Criteria) this;
        }

        public Criteria andUserSerialLessThan(Long value) {
            addCriterion("user_serial <", value, "userSerial");
            return (Criteria) this;
        }

        public Criteria andUserSerialLessThanOrEqualTo(Long value) {
            addCriterion("user_serial <=", value, "userSerial");
            return (Criteria) this;
        }

        public Criteria andUserSerialIn(List<Long> values) {
            addCriterion("user_serial in", values, "userSerial");
            return (Criteria) this;
        }

        public Criteria andUserSerialNotIn(List<Long> values) {
            addCriterion("user_serial not in", values, "userSerial");
            return (Criteria) this;
        }

        public Criteria andUserSerialBetween(Long value1, Long value2) {
            addCriterion("user_serial between", value1, value2, "userSerial");
            return (Criteria) this;
        }

        public Criteria andUserSerialNotBetween(Long value1, Long value2) {
            addCriterion("user_serial not between", value1, value2, "userSerial");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("user_name is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("user_name is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("user_name =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("user_name <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("user_name >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_name >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("user_name <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("user_name <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("user_name like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("user_name not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("user_name in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("user_name not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("user_name between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("user_name not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserHeadIsNull() {
            addCriterion("user_head is null");
            return (Criteria) this;
        }

        public Criteria andUserHeadIsNotNull() {
            addCriterion("user_head is not null");
            return (Criteria) this;
        }

        public Criteria andUserHeadEqualTo(String value) {
            addCriterion("user_head =", value, "userHead");
            return (Criteria) this;
        }

        public Criteria andUserHeadNotEqualTo(String value) {
            addCriterion("user_head <>", value, "userHead");
            return (Criteria) this;
        }

        public Criteria andUserHeadGreaterThan(String value) {
            addCriterion("user_head >", value, "userHead");
            return (Criteria) this;
        }

        public Criteria andUserHeadGreaterThanOrEqualTo(String value) {
            addCriterion("user_head >=", value, "userHead");
            return (Criteria) this;
        }

        public Criteria andUserHeadLessThan(String value) {
            addCriterion("user_head <", value, "userHead");
            return (Criteria) this;
        }

        public Criteria andUserHeadLessThanOrEqualTo(String value) {
            addCriterion("user_head <=", value, "userHead");
            return (Criteria) this;
        }

        public Criteria andUserHeadLike(String value) {
            addCriterion("user_head like", value, "userHead");
            return (Criteria) this;
        }

        public Criteria andUserHeadNotLike(String value) {
            addCriterion("user_head not like", value, "userHead");
            return (Criteria) this;
        }

        public Criteria andUserHeadIn(List<String> values) {
            addCriterion("user_head in", values, "userHead");
            return (Criteria) this;
        }

        public Criteria andUserHeadNotIn(List<String> values) {
            addCriterion("user_head not in", values, "userHead");
            return (Criteria) this;
        }

        public Criteria andUserHeadBetween(String value1, String value2) {
            addCriterion("user_head between", value1, value2, "userHead");
            return (Criteria) this;
        }

        public Criteria andUserHeadNotBetween(String value1, String value2) {
            addCriterion("user_head not between", value1, value2, "userHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadIsNull() {
            addCriterion("content_head is null");
            return (Criteria) this;
        }

        public Criteria andContentHeadIsNotNull() {
            addCriterion("content_head is not null");
            return (Criteria) this;
        }

        public Criteria andContentHeadEqualTo(String value) {
            addCriterion("content_head =", value, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadNotEqualTo(String value) {
            addCriterion("content_head <>", value, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadGreaterThan(String value) {
            addCriterion("content_head >", value, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadGreaterThanOrEqualTo(String value) {
            addCriterion("content_head >=", value, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadLessThan(String value) {
            addCriterion("content_head <", value, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadLessThanOrEqualTo(String value) {
            addCriterion("content_head <=", value, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadLike(String value) {
            addCriterion("content_head like", value, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadNotLike(String value) {
            addCriterion("content_head not like", value, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadIn(List<String> values) {
            addCriterion("content_head in", values, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadNotIn(List<String> values) {
            addCriterion("content_head not in", values, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadBetween(String value1, String value2) {
            addCriterion("content_head between", value1, value2, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentHeadNotBetween(String value1, String value2) {
            addCriterion("content_head not between", value1, value2, "contentHead");
            return (Criteria) this;
        }

        public Criteria andContentMainIsNull() {
            addCriterion("content_main is null");
            return (Criteria) this;
        }

        public Criteria andContentMainIsNotNull() {
            addCriterion("content_main is not null");
            return (Criteria) this;
        }

        public Criteria andContentMainEqualTo(String value) {
            addCriterion("content_main =", value, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentMainNotEqualTo(String value) {
            addCriterion("content_main <>", value, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentMainGreaterThan(String value) {
            addCriterion("content_main >", value, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentMainGreaterThanOrEqualTo(String value) {
            addCriterion("content_main >=", value, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentMainLessThan(String value) {
            addCriterion("content_main <", value, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentMainLessThanOrEqualTo(String value) {
            addCriterion("content_main <=", value, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentMainLike(String value) {
            addCriterion("content_main like", value, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentMainNotLike(String value) {
            addCriterion("content_main not like", value, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentMainIn(List<String> values) {
            addCriterion("content_main in", values, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentMainNotIn(List<String> values) {
            addCriterion("content_main not in", values, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentMainBetween(String value1, String value2) {
            addCriterion("content_main between", value1, value2, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentMainNotBetween(String value1, String value2) {
            addCriterion("content_main not between", value1, value2, "contentMain");
            return (Criteria) this;
        }

        public Criteria andContentImgIsNull() {
            addCriterion("content_img is null");
            return (Criteria) this;
        }

        public Criteria andContentImgIsNotNull() {
            addCriterion("content_img is not null");
            return (Criteria) this;
        }

        public Criteria andContentImgEqualTo(String value) {
            addCriterion("content_img =", value, "contentImg");
            return (Criteria) this;
        }

        public Criteria andContentImgNotEqualTo(String value) {
            addCriterion("content_img <>", value, "contentImg");
            return (Criteria) this;
        }

        public Criteria andContentImgGreaterThan(String value) {
            addCriterion("content_img >", value, "contentImg");
            return (Criteria) this;
        }

        public Criteria andContentImgGreaterThanOrEqualTo(String value) {
            addCriterion("content_img >=", value, "contentImg");
            return (Criteria) this;
        }

        public Criteria andContentImgLessThan(String value) {
            addCriterion("content_img <", value, "contentImg");
            return (Criteria) this;
        }

        public Criteria andContentImgLessThanOrEqualTo(String value) {
            addCriterion("content_img <=", value, "contentImg");
            return (Criteria) this;
        }

        public Criteria andContentImgLike(String value) {
            addCriterion("content_img like", value, "contentImg");
            return (Criteria) this;
        }

        public Criteria andContentImgNotLike(String value) {
            addCriterion("content_img not like", value, "contentImg");
            return (Criteria) this;
        }

        public Criteria andContentImgIn(List<String> values) {
            addCriterion("content_img in", values, "contentImg");
            return (Criteria) this;
        }

        public Criteria andContentImgNotIn(List<String> values) {
            addCriterion("content_img not in", values, "contentImg");
            return (Criteria) this;
        }

        public Criteria andContentImgBetween(String value1, String value2) {
            addCriterion("content_img between", value1, value2, "contentImg");
            return (Criteria) this;
        }

        public Criteria andContentImgNotBetween(String value1, String value2) {
            addCriterion("content_img not between", value1, value2, "contentImg");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousIsNull() {
            addCriterion("is_anonymous is null");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousIsNotNull() {
            addCriterion("is_anonymous is not null");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousEqualTo(Integer value) {
            addCriterion("is_anonymous =", value, "isAnonymous");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousNotEqualTo(Integer value) {
            addCriterion("is_anonymous <>", value, "isAnonymous");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousGreaterThan(Integer value) {
            addCriterion("is_anonymous >", value, "isAnonymous");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_anonymous >=", value, "isAnonymous");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousLessThan(Integer value) {
            addCriterion("is_anonymous <", value, "isAnonymous");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousLessThanOrEqualTo(Integer value) {
            addCriterion("is_anonymous <=", value, "isAnonymous");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousIn(List<Integer> values) {
            addCriterion("is_anonymous in", values, "isAnonymous");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousNotIn(List<Integer> values) {
            addCriterion("is_anonymous not in", values, "isAnonymous");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousBetween(Integer value1, Integer value2) {
            addCriterion("is_anonymous between", value1, value2, "isAnonymous");
            return (Criteria) this;
        }

        public Criteria andIsAnonymousNotBetween(Integer value1, Integer value2) {
            addCriterion("is_anonymous not between", value1, value2, "isAnonymous");
            return (Criteria) this;
        }

        public Criteria andFunnyNumIsNull() {
            addCriterion("funny_num is null");
            return (Criteria) this;
        }

        public Criteria andFunnyNumIsNotNull() {
            addCriterion("funny_num is not null");
            return (Criteria) this;
        }

        public Criteria andFunnyNumEqualTo(Integer value) {
            addCriterion("funny_num =", value, "funnyNum");
            return (Criteria) this;
        }

        public Criteria andFunnyNumNotEqualTo(Integer value) {
            addCriterion("funny_num <>", value, "funnyNum");
            return (Criteria) this;
        }

        public Criteria andFunnyNumGreaterThan(Integer value) {
            addCriterion("funny_num >", value, "funnyNum");
            return (Criteria) this;
        }

        public Criteria andFunnyNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("funny_num >=", value, "funnyNum");
            return (Criteria) this;
        }

        public Criteria andFunnyNumLessThan(Integer value) {
            addCriterion("funny_num <", value, "funnyNum");
            return (Criteria) this;
        }

        public Criteria andFunnyNumLessThanOrEqualTo(Integer value) {
            addCriterion("funny_num <=", value, "funnyNum");
            return (Criteria) this;
        }

        public Criteria andFunnyNumIn(List<Integer> values) {
            addCriterion("funny_num in", values, "funnyNum");
            return (Criteria) this;
        }

        public Criteria andFunnyNumNotIn(List<Integer> values) {
            addCriterion("funny_num not in", values, "funnyNum");
            return (Criteria) this;
        }

        public Criteria andFunnyNumBetween(Integer value1, Integer value2) {
            addCriterion("funny_num between", value1, value2, "funnyNum");
            return (Criteria) this;
        }

        public Criteria andFunnyNumNotBetween(Integer value1, Integer value2) {
            addCriterion("funny_num not between", value1, value2, "funnyNum");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumIsNull() {
            addCriterion("notfunny_num is null");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumIsNotNull() {
            addCriterion("notfunny_num is not null");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumEqualTo(Integer value) {
            addCriterion("notfunny_num =", value, "notfunnyNum");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumNotEqualTo(Integer value) {
            addCriterion("notfunny_num <>", value, "notfunnyNum");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumGreaterThan(Integer value) {
            addCriterion("notfunny_num >", value, "notfunnyNum");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("notfunny_num >=", value, "notfunnyNum");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumLessThan(Integer value) {
            addCriterion("notfunny_num <", value, "notfunnyNum");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumLessThanOrEqualTo(Integer value) {
            addCriterion("notfunny_num <=", value, "notfunnyNum");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumIn(List<Integer> values) {
            addCriterion("notfunny_num in", values, "notfunnyNum");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumNotIn(List<Integer> values) {
            addCriterion("notfunny_num not in", values, "notfunnyNum");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumBetween(Integer value1, Integer value2) {
            addCriterion("notfunny_num between", value1, value2, "notfunnyNum");
            return (Criteria) this;
        }

        public Criteria andNotfunnyNumNotBetween(Integer value1, Integer value2) {
            addCriterion("notfunny_num not between", value1, value2, "notfunnyNum");
            return (Criteria) this;
        }

        public Criteria andCommentNumIsNull() {
            addCriterion("comment_num is null");
            return (Criteria) this;
        }

        public Criteria andCommentNumIsNotNull() {
            addCriterion("comment_num is not null");
            return (Criteria) this;
        }

        public Criteria andCommentNumEqualTo(Integer value) {
            addCriterion("comment_num =", value, "commentNum");
            return (Criteria) this;
        }

        public Criteria andCommentNumNotEqualTo(Integer value) {
            addCriterion("comment_num <>", value, "commentNum");
            return (Criteria) this;
        }

        public Criteria andCommentNumGreaterThan(Integer value) {
            addCriterion("comment_num >", value, "commentNum");
            return (Criteria) this;
        }

        public Criteria andCommentNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("comment_num >=", value, "commentNum");
            return (Criteria) this;
        }

        public Criteria andCommentNumLessThan(Integer value) {
            addCriterion("comment_num <", value, "commentNum");
            return (Criteria) this;
        }

        public Criteria andCommentNumLessThanOrEqualTo(Integer value) {
            addCriterion("comment_num <=", value, "commentNum");
            return (Criteria) this;
        }

        public Criteria andCommentNumIn(List<Integer> values) {
            addCriterion("comment_num in", values, "commentNum");
            return (Criteria) this;
        }

        public Criteria andCommentNumNotIn(List<Integer> values) {
            addCriterion("comment_num not in", values, "commentNum");
            return (Criteria) this;
        }

        public Criteria andCommentNumBetween(Integer value1, Integer value2) {
            addCriterion("comment_num between", value1, value2, "commentNum");
            return (Criteria) this;
        }

        public Criteria andCommentNumNotBetween(Integer value1, Integer value2) {
            addCriterion("comment_num not between", value1, value2, "commentNum");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}