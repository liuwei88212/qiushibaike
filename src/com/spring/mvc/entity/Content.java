package com.spring.mvc.entity;

import com.spring.mvc.common.DateUtil;

public class Content {
    private Integer id;
    
    private Long articleId;

    private Long articleTime;
    private String articleTimeStr;

    private String articleFrom;
    private Long userSerial;

    private String userName;

    private String userHead;

    private String contentHead;

    private String contentMain;

    private String contentImg;

    private Integer isAnonymous;
    
    private Integer funnyNum;

    private Integer notfunnyNum;

    private Integer commentNum;

	public String getArticleTimeStr() {
		if(articleTime!=null){
			articleTimeStr = DateUtil.formatLongToString(articleTime);
		}
		return articleTimeStr;
	}
	
    public Long getArticleId() {
		return articleId;
	}

	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}

	public Long getArticleTime() {
		return articleTime;
	}

	public void setArticleTime(Long articleTime) {
		this.articleTime = articleTime;
	}

	public String getArticleFrom() {
		return articleFrom;
	}

	public void setArticleFrom(String articleFrom) {
		this.articleFrom = articleFrom;
	}

	public Integer getFunnyNum() {
		return funnyNum;
	}

	public void setFunnyNum(Integer funnyNum) {
		this.funnyNum = funnyNum;
	}

	public Integer getNotfunnyNum() {
		return notfunnyNum;
	}

	public void setNotfunnyNum(Integer notfunnyNum) {
		this.notfunnyNum = notfunnyNum;
	}

	public Integer getCommentNum() {
		return commentNum;
	}

	public void setCommentNum(Integer commentNum) {
		this.commentNum = commentNum;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getUserSerial() {
        return userSerial;
    }

    public void setUserSerial(Long userSerial) {
        this.userSerial = userSerial;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserHead() {
        return userHead;
    }

    public void setUserHead(String userHead) {
        this.userHead = userHead == null ? null : userHead.trim();
    }

    public String getContentHead() {
        return contentHead;
    }

    public void setContentHead(String contentHead) {
        this.contentHead = contentHead == null ? null : contentHead.trim();
    }

    public String getContentMain() {
        return contentMain;
    }

    public void setContentMain(String contentMain) {
        this.contentMain = contentMain == null ? null : contentMain.trim();
    }

    public String getContentImg() {
        return contentImg;
    }

    public void setContentImg(String contentImg) {
        this.contentImg = contentImg == null ? null : contentImg.trim();
    }

    public Integer getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(Integer isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

	public void setArticleTimeStr(String articleTimeStr) {
		this.articleTimeStr = articleTimeStr;
	}
    
    
}