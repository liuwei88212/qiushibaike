<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
<c:forEach items="${page.content}" var="content" varStatus="s">
	<div id="qiushi_tag_${content.articleId }" class="article block untagged mb15">
		<c:if test="${content.isAnonymous==1 }">
			<div class="author clearfix">
				<a href="http://www.qiushibaike.com/users/${content.userSerial}">
					<img alt="${content.userName }" src="${content.userHead }" />
				</a> 
				<a href="http://www.qiushibaike.com/users/${content.userSerial}">${content.userName}</a> <a class="send-msg"
					href="http://www.qiushibaike.com/users/${content.userSerial}#?to=${content.userSerial}&amp;from=79745187&amp;login=${content.userName }&amp;icon=None">发小纸条</a>
			</div>
		</c:if>
		<div class="content" title="${content.articleTimeStr }">
			<span>No.${(page.size*page.number)+(s.index+1)}&nbsp; </span>
			${content.contentMain }
		</div>
		<c:if test="${content.contentImg!=null}">
			<div class="thumb">
				<a href="${content.contentImg }"> 
					<img alt="糗事#${content.articleId}" src="${content.contentImg}" />
				</a>
			</div>
		</c:if>
		<div class="stats clearfix">
			<span>
				${content.articleTimeStr }
			</span>
			<span class="dash">|</span> 
			<span class="stats-vote">
				<i class="number">${content.funnyNum}</i> 好笑
			</span> 
			<span class="dash">|</span>
			<span class="stats-down">
				<i class="number">${content.notfunnyNum}</i> 不好笑
			</span>
			<span class="dash">|</span>
			<span class="stats-comments"> 
				<a onclick="_hmt.push(['_trackEvent', 'post', 'click', 'signlePost'])" target="_blank" title="${content.commentNum }条评论" class="qiushi_comments"
					id="c-79604013" href="/article/79604013?list=8hr&amp;s=4685869">
					<i class="number">${content.commentNum}</i> 
					回复
				</a>
			</span>
		</div>
	</div>
</c:forEach>
