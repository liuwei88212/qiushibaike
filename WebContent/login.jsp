<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div id="login-form" class="signin-box fadeInUp animated" style="left: 50%; margin-left: -180px; z-index: 1002; position: absolute; top: 40px; margin-top: 0px; display: none;">
<div class="signin-account clearfix">
<h4 class="social-signin-heading">使用社交账号登录：</h4>
<a onclick="_hmt.push(['_trackEvent', 'login', 'click', 'weibo'])" class="social-btn social-weibo" href="https://api.weibo.com/oauth2/authorize?client_id=63372306&amp;redirect_uri=http%3A%2F%2Fwww.qiushibaike.com%2Fsession">新浪微博</a>
<a onclick="_hmt.push(['_trackEvent', 'login', 'click', 'qq'])" class="social-btn social-qq" href="http://openapi.qzone.qq.com/oauth/show?which=Login&amp;display=pc&amp;client_id=100251437&amp;response_type=code&amp;redirect_uri=www.qiushibaike.com/session?src=qq">腾讯QQ</a>
</div>
<div class="signin-form clearfix">
<form action="#" method="post">
<div class="signin-section clearfix">
<input type="text" placeholder="昵称或邮箱" name="login" class="form-input form-input-first">
<input type="password" placeholder="密码" name="password" class="form-input">
<input type="checkbox" style="display:none" value="checked" checked="" name="remember_me" id="remember_me">
</div>
<div id="signin-error" class="signin-error"></div>
<button onclick="_hmt.push(['_trackEvent', 'login', 'click', 'qiubai'])" class="form-submit" id="form-submit" type="submit">登录</button>
</form>
</div>
<div class="signin-foot clearfix">
<a onclick="_hmt.push(['_trackEvent', 'login', 'click', 'fetchpass'])" class="fetch-password" href="/new4/fetchpass">找回密码</a>
<a onclick="_hmt.push(['_trackEvent', 'login', 'click', 'signup'])" href="/new4/signup">注册</a>
</div>
</div>


<div class="lb_overlay js_lb_overlay" 
	style="display: none;height: 10751px; position: absolute; width: 100%; top: 0px; left: 0px; right: 0px; bottom: 0px; z-index: 1001; background: none repeat scroll 0% 0% black; opacity: 0.6;"></div>

