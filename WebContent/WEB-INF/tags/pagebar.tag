<%@tag pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="page" type="org.springframework.data.domain.Page" required="true"%>
<%@ attribute name="actionURL" type="java.lang.String" required="true" %>

<div class="pagebar clearfix">
	<a title="上一页" href="javascript:void(0);" class='prev <c:if test="${(page.number+1)==1 }">disabled</c:if>'>
		<i class="iconfont"> &lt; </i>
	</a>
	<c:forEach step="1" begin="1" end="${page.totalPages}" var="pageNum">
		<c:choose>
			<c:when test="${pageNum == (page.number+1)}">
				<span class="current">${pageNum}</span>
			</c:when>
			<c:otherwise>
				<c:if test="${pageNum < 5}">
					<a title="第${pageNum}页" href="${actionURL}&pageNum=${pageNum}"> 
						${pageNum} 
					</a>
				</c:if>
				<c:if test="${pageNum == 5}">
					<span class="dots">…</span>
				</c:if>
				<c:if test="${pageNum == (page.totalPages-1)}">
					<a title="第${pageNum}页" href="${actionURL}&pageNum=${pageNum}"> 
						${pageNum} 
					</a>
				</c:if>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	
	<c:if test="${page.number < page.totalPages}">
		<a title="下一页" href="${actionURL}&pageNum=${page.number+2}" class="next"> 
			下一页 
			<i class="iconfont"> &gt; </i>
		</a>
	</c:if>
	<!-- 
	<div class="tips clearfix">
		<span class="text"> 按方向键翻页 </span> <span
			class="key key-left-white" title="左方向键"> ← </span> <span
			class="key key-right-white" title="右方向键"> → </span>
	</div>
	 -->
</div>