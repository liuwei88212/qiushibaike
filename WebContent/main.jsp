<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Window-target" content="_top" />
<meta name="author" content="BruceLiu" />
<meta name="description" content="糗事百科个人版-by BruceLiu" />
<meta name="keywords" content="糗事百科,糗事百科个人版,BruceLiu,布鲁斯刘" />
<meta name="robots" content="all" /> 

<title>【糗事百科-个人版】</title>
<link href="<c:url value='/favicon.ico'/>" type="image/x-icon" rel="shortcut icon" />
<link href="<%=request.getContextPath() %>/css/font-awesome.min.css" rel="stylesheet"  type="text/css"/>
<link href="<%=request.getContextPath() %>/css/layout.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/layout.pagebar.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/layout.menu.css" rel="stylesheet" type="text/css" />
<script src="<%=request.getContextPath() %>/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/NeuF.ScrollPage.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){
		//菜单初始化
		var from = "<%=request.getAttribute("from")%>";
		$(".menu-bar .menu li").each(function(){
			var rel = $(this).children().first().attr("rel");
			if(from==rel){
				$(this).children().first().attr("id","highlight");
			}
		});
	});
	/**回顶部和底部*/
	$(document).ready(function(){
		var height = $(window).height(); 
		$("#backToTop1").show(200, function(){
            $("#backToTop1").css({
                top: height - 100
            });
        });
	    $(window).scroll( function() {               //滚动时触发
	        var top = $(document).scrollTop();       //获取滚动条到顶部的垂直高度
	        var height = $(window).height();         //获得可视浏览器的高度
	        if(top > 100){
	            $("#backToTop1").show(200, function(){
	                $("#backToTop1").css({
	                    top: height + top - 100
	                });
	            });
	        }
	    });
	    /*点击回到顶部*/
	    $('#backToTop-up').click(function(){
	        $('html, body').animate({
	            scrollTop: 0
	        }, 500);
	    });
	    /*点击到底部*/
	    $('#backToTop-down').click(function(){
	        $('html, body').animate({
	            scrollTop: $(document).height()
	        }, 500);
	    });
	});
	
	 /**滚动加载 */
	 $(function () {
         window.scrollTo(0, 0); //每次F5刷新把滚动条置顶
         //marginBottom表示滚动条离底部的距离，0表示滚动到最底部才加载，可以根据需要修改  
         new NeuF.ScrollPage(window, { delay: 1000, marginBottom: 0 }, function (offset) {
             if (offset > 0) {
                 $("#Loadding").show(); //加载提示
                 setTimeout(function () {
                     //这里就是异步获取内容的地方，这里简化成一句话，可以根据需要修改
                     //$("#content-left").append($("<div class='content'>第“" + ($(".content").size() + 1) + "”页内容</div>"));
                     //内容获取后，隐藏加载提示
                     //$("#Loadding").hide();
                     var pageNum = $(".loadContent").size() + 1;
                     var load = $("<div class='loadContent'></div>").load("QiubaiLoadServlet?pageNum="+pageNum+"&from=<%=request.getAttribute("from")%>");
                     $("#content-left").append(load);
                     $("#Loadding").hide();
                 }, 1000);
             }
         });
     });
	
	/**菜单固定*/
	window.onload = function() {
		var nav = document.getElementById('menu');
		var navFixed = document.getElementById('navFixed');
		window.onscroll = function() {
			var scrollTop = document.documentElement.scrollTop
					|| document.body.scrollTop;
// 			document.title = scrollTop+","+nav.offsetTop;
			if (scrollTop > nav.offsetTop) {
				navFixed.style.display = 'block';
			} else if (scrollTop <= nav.offsetTop) {
				navFixed.style.display = 'none';
			}
		};
	};
</script>
<style type="text/css">
	 div#Loadding { text-align: center; margin-top: 10px; display: none; font-weight: bold; color:Red; }
	 div#loadContent {margin-bottom:15px;}
</style>
</head>
<body>
	<input id="hiddenFrom" value="${from}" type="hidden">
	<div id="container">
		<div id="header">
			<div class="content-block">
				<div class="logo">
					<a href="http://www.qiushibaike.com/"></a>
				</div>
			</div>
		</div>
		<br class="clearfloat" />
		<jsp:include page="menu.jsp" />

		<br class="clearfloat" />
		<div id="mainContent">
			<jsp:include page="sidebar.jsp" />
		</div>
		<div id="content" class="main">
			<div class="content-block clearfix">
				<div id="content-left" class="col1">
					<div class="loadContent">
					<c:forEach items="${page.content}" var="content" varStatus="s">
						<div id="qiushi_tag" class="article block untagged mb15">
							<c:if test="${content.isAnonymous==1 }">
								<div class="author clearfix">
									<a href="UserController?method=detail&userId=${content.userSerial}"> 
										<img alt="${content.userName }" src="${content.userHead }" />
									</a>
									<a href="UserController?method=detail&userId=${content.userSerial}">${content.userName }</a>
									<a class="send-msg" href="http://www.qiushibaike.com/users/${content.userSerial}#?to=${content.userSerial}&amp;from=79745187&amp;login=${content.userName }&amp;icon=None">
										发小纸条
									</a>
								</div>
							</c:if>
							<div class="content" title="${content.articleTimeStr }">
								<span>No.${(page.size*page.number)+(s.index+1)}&nbsp;</span>
								${content.contentMain }
							</div>
							<c:if test="${content.contentImg!=null}">
								<div class="thumb">
									<a href="${content.contentImg }">
										<img alt="糗事#79614909" src="${content.contentImg}" />
									</a>
								</div>
							</c:if>
							<div class="stats clearfix">
								<span>
									${content.articleTimeStr }
								</span>
								<span class="dash">|</span> 
								<span class="stats-vote">
									<i class="number">${content.funnyNum}</i> 好笑
								</span> 
								<span class="dash">|</span>
								<span class="stats-down">
									<i class="number">${content.notfunnyNum}</i> 不好笑
								</span>
								<span class="dash">|</span>
								<span class="stats-comments"> 
									<a onclick="_hmt.push(['_trackEvent', 'post', 'click', 'signlePost'])" target="_blank" title="${content.commentNum }条评论" class="qiushi_comments"
										id="c-79604013" href="/article/79604013?list=8hr&amp;s=4685869">
										<i class="number">${content.commentNum}</i> 
										回复
									</a>
								</span>
							</div>
						</div>
					</c:forEach>
					</div>
				</div><!-- end content-left -->
				<div id="Loadding">
			               正在加载，请稍候 ...
				</div>
				<!-- <tags:pagebar actionURL="QiubaiController?from=8hr" page="${page}"/> -->
			</div><!-- end content block -->
		</div><!-- end content -->
	</div>
	
	<!--backtoTop1-->
    <!--兼容所有现代浏览器同时包括 ie6/7/8/9 （ie6会有一点点抖动）-->
    <div class="backtoTop" id="backToTop1">
        <div id="backToTop-up" class="up-back"><i class="fa fa-angle-up"></i></div>
        <div id="backToTop-down" class="down-back"><i class="fa fa-angle-down"></i></div>
    </div>
	<br class="clearfloat" />
	<jsp:include page="footer.jsp" />
</body>
</html>