window.inspector = {
    pool: [],
    idx: 0,
    curid: 0,
    kTimeMark: 0,
    fetch: function() {
        var self = this;
        var pool = self.pool;
        $.get('/new3/fetch?t=' + Math.floor(Math.random() * 5000 + 10000), function(list) {
            if (list instanceof Array) {
                list = self.shuffle(list);
                var ismore = self.append_list(list);
                if (self.idx <= 0 || self.idx >= pool.length) {
                    self.forward();
                }
            }
        });
    },
    shuffle: function(o) {
        if (!(o && o instanceof Array)) return o;
        for (var i = 0, len = o.length; i < o.length; i++) {
            var index = parseInt(Math.random() * len, 10);
            var tmp = o[index];
            o[index] = o[i];
            o[i] = tmp;
        }
        return o;
    },
    append_list: function(list) {
        var self = this,
            pool = self.pool,
            len = pool.length,
            i, j;
        if (!list) return false;
        for (i = 0; i < list.length; i++) {
            for (j = 0; j < len; j++) {
                if (list[i]["id"] == pool[j]["id"]) {
                    break;
                }
            }
            if (j >= len) pool.push(list[i]);
        }
        return pool.length > len;
    },
    parse: function(raw) {
        var parsePic = function(img) {
            var pic = Base64.decode(img).replace(/\.\w+$/, ".jpg");
            var aid = parseInt(pic.replace(/\D/g, ''),10);
            return "<img src='http://pic.qiushibaike.com/system/pictures/" + Math.floor(aid / 10000) + '/' + aid + "/medium/" + Base64.decode(img) + "' id='article-pic' />";
        };
        return {
            content: Base64.decode(raw.content),
            tag: raw.tags.length > 0 ? "标签：" + Base64.decode(raw.tags) : '',
            id: raw.id,
            pic: raw.img.length > 0 ? parsePic(raw.img) : ''
        }
    },   
    forward: function() {
        var self = this;
        var pool = self.pool;
        if (self.idx < pool.length) {
            $('.content').addClass('loading');
            var article = self.parse(pool[self.idx]);
            if (article.content.length > 0) $("#content").html(article.content.replace(/\r\n/g, "<br />"));
            $("#tag").html(article.tag);
            $("#pic").html(article.pic);
            self.curid = article.id;
            self.idx ++;
            self.kTimeMark = new Date().getTime();
        }
        if (article.pic != '') {
            $('#article-pic').load(function(){
                $('.content').removeClass('loading');
            });
        } else {
            $('.content').removeClass('loading');
        }
        if (self.idx + 3 > pool.length) {
            self.fetch();
        }
    },
    vote: {
        update: function(action) {
            var self = window.inspector;
            $.get('/new3/inspect2/' + self.curid + '/' + action + "?time=" + (new Date().getTime() - self.kTimeMark) / 1000);
            self.forward();
            self.vote.buttons.disable();
        },
        buttons: {
            active: function() {
                $('a.voteme').removeClass('disable');
            },
            disable: function() {
                $('a.voteme').addClass('disable');
                setTimeout(function() {
                    window.inspector.vote.buttons.active();
                }, 1000);
            }
        },
        bind: function(dom) {
            var self = window.inspector;
            $(dom).click(function() {
                if ($(this).hasClass('disable')) {
                    return false;
                } else {
                    self.vote.update(parseInt($(this).data('vote'), 10));
                }
            });
        },
        init: function() {
            window.inspector.vote.bind('a.voteme');
        }
    },
    helper: function() {
        $("#a-suggest").click(function() {
            $("#feedback").lightbox_me({
                centered: true
            });
        });
        $("#form-suggest").submit(function(e) {
            e.preventDefault();
            var form = $(this);
            var field = form.find('textarea');
            var text = field.val();
            field.val("[瀹℃牳]" + text);
            $.post(form.attr('action'), form.serialize(), function(ret) {
                $("#feedback").trigger('close');
                field.val("");
            });
            return false;
        });
    },
    init: function() {
        this.fetch();
        this.vote.init();
        this.helper();
    }
}
/**
$(document).ready(function() {
    window.inspector.init();
});
*/