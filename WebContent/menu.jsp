<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="menu" class="menu-bar">
	<div class="menu clearfix" id="nav">
		<ul>
			<li class="menuout"><a href="<%=request.getContextPath() %>/QiubaiController?from=8hr" rel="8hr">热门</a></li>
			<li class="menuout" onmouseout="this.className='menuout'" onmouseover="this.className='menuover'">
				<a class="submenutitle" href="<%=request.getContextPath() %>/QiubaiController?from=hot" rel="hot">精华</a>
				<a class="submenu" href="<%=request.getContextPath() %>/QiubaiController?from=hot" rel="hot">24小时内</a>
				<a class="submenu" href="<%=request.getContextPath() %>/QiubaiController?from=week" rel="week">7天内</a>
				<a class="submenu" href="<%=request.getContextPath() %>/QiubaiController?from=month" rel="month">30天内</a>
			</li>
			<li onmouseout="this.className='menuout'" onmouseover="this.className='menuover'" class="menuout">
				<a class="submenutitle" href="<%=request.getContextPath() %>/QiubaiController?from=imgrank" rel="imgrank">真相</a>
				<a class="submenu" href="<%=request.getContextPath() %>/QiubaiController?from=imgrank" rel="imgrank">硬菜</a>
				<a class="submenu" href="<%=request.getContextPath() %>/QiubaiController?from=pic" rel="pic">时令</a>
			</li>
			<li class="menuout"><a href="<%=request.getContextPath() %>/QiubaiController?from=late" rel="late">最新</a></li>
			<li class="menuout"><a href="<%=request.getContextPath() %>/news?from=news" rel="news">审帖</a></li>
			<li class="menuout"><a href="<%=request.getContextPath() %>/user/add.jsp?from=rel" rel="add">投稿</a></li>
			
			<li class="menuout"><a href="<%=request.getContextPath() %>/app/QiuBaiServlet?from=app" rel="app">APP</a></li>
		</ul>
	</div>
	<div class="userbar">
		<div class="login" style="display: block;">
			<a title="个人中心" class="username" href="/my">转角遇见你妹</a>
			<a title="退出" class="exit" href="/logout">退出</a>
			<a title="小纸条" class="message" href="/my?messager=open">
				<span class="bti"></span>
				<span id="unread_messages_count" style="display: none;"></span>
			</a>
		</div>
		<div class="logout" style="display: none;">
			<a id="logintop" class="fn-signin-required logintop" href="javascript:void(0);">登录</a>
		</div>
	</div>
</div>
<div class="menu-bar-fixed" id="navFixed" style="display: none;">
	<div class="menu">
		<ul>
			<li class="menuout"><a id="highlight" href="<%=request.getContextPath() %>/QiubaiController?from=8hr" rel="8hr">热门</a></li>
			<li class="menuout" onmouseout="this.className='menuout'" onmouseover="this.className='menuover'">
				<a class="submenutitle" href="<%=request.getContextPath() %>/QiubaiController?from=hot" rel="hot">精华</a>
				<a class="submenu" href="<%=request.getContextPath() %>/QiubaiController?from=hot" rel="hot">24小时内</a>
				<a class="submenu" href="<%=request.getContextPath() %>/QiubaiController?from=week" rel="week">7天内</a>
				<a class="submenu" href="<%=request.getContextPath() %>/QiubaiController?from=month" rel="month">30天内</a>
			</li>
			<li class="menuout" onmouseout="this.className='menuout'" onmouseover="this.className='menuover'">
				<a class="submenutitle" href="<%=request.getContextPath() %>/QiubaiController?from=imgrank" rel="imgrank">真相</a>
				<a class="submenu" href="<%=request.getContextPath() %>/QiubaiController?from=imgrank" rel="imgrank">硬菜</a>
				<a class="submenu" href="<%=request.getContextPath() %>/QiubaiController?from=pic" rel="pic">时令</a>
			</li>
			<li class="menuout"><a href="<%=request.getContextPath() %>/QiubaiController?from=late" rel="late">最新</a></li>
			<li class="menuout"><a href="<%=request.getContextPath() %>/news?from=news" rel="news">审帖</a></li>
			<li class="menuout"><a href="<%=request.getContextPath() %>/user/add.jsp?from=add" rel="add">投稿</a></li>
		</ul>
	</div>
	<div class="userbar">
		<div class="login" style="display: block;">
			<a title="个人中心" class="username" href="/my">转角遇见你妹</a>
			<a title="退出" class="exit" href="/logout">退出</a>
			<a title="小纸条" class="message" href="/my?messager=open">
				<span class="bti"></span>
				<span id="unread_messages_count" style="display: none;"></span>
			</a>
		</div>
		<div class="logout" style="display: none;">
			<a id="logintop" class="fn-signin-required logintop" href="javascript:void(0);">登录</a>
		</div>
	</div>
</div>