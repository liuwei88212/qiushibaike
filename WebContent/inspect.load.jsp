<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>		
<c:forEach items="${page.content}" var="news" varStatus="s">
	<div class="main">
		<div class="content">
			<p id="content">
				<span>No.${(page.size*page.number)+(s.index+1)}&nbsp;</span>
				
				${news.content }
			</p>
			<a id="pic">${news.img }</a>
			<p id="tag">${news.tags }</p>
		</div>
		<div class="option">
			<a class="up voteme"><span></span>好笑</a> <a class="down voteme"><span></span>不给力</a>
			<a class="report voteme"><span></span>滚粗</a> <a class="pass voteme">说不清楚，跳过</a>
		</div>
	</div>
</c:forEach>