<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>jQuery Mobile基本页面结构</title>
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0a3/jquery.mobile-1.0a3.min.css" />
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.5.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/mobile/1.0a3/jquery.mobile-1.0a3.min.js"></script>
    </head>
    <body>
        <div data-role="page" id="home">
            <div data-role="header"><h1>Home</h1></div>
            <div data-role="content">
            	<p>
            	 	<a href="list.jsp" data-role="button" >List</a>
            		<a href="form.jsp" data-role="button" >Form</a>
            		<a href="#about" data-role="button" data-transition="flip">About</a>
            	</p>
            </div>
        </div>
        <div data-role="page" id="about">
        	<div data-role="header"><h1>About This App</h1></div>
            <div data-role="content">
            	<div data-role="collapsible" data-state="collapsed">
                   <h3>About this app</h3>
                   <p>This app rocks!</p>
               </div>
               <a href="#home">Go home</a>
            </div>
        </div>
    </body>
</html>