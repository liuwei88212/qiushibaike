<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Cache-control" content="max-age=120" />
<meta name="viewport" content="width=device-width; initial-scale=1.3; minimum-scale=1.0; maximum-scale=2.0" />
<meta name="MobileOptimized" content="240" />
<meta name="format-detection" content="telephone=no" />
<link href="${ctx}/mobile/css/layout.css" rel="stylesheet" type="text/css" />
<title>糗事百科</title>
<style type="text/css">
</style>
</head>
<body class="index">
	<div id="logo">
	   <a href=""><img src="${ctx }/mobile/static/images/qiushi/uc-logo.gif" /></a>
	   <a href="add">投稿</a>&nbsp;|&nbsp;<a href="login">登录</a>
	</div>
	<div id="nav">
		<a href="<%=request.getContextPath() %>/app/QiuBaiServlet?from=8hr" class="current">8小时最糗</a>&nbsp;|&nbsp;
		<a href="<%=request.getContextPath() %>/app/QiuBaiServlet?from=hot">今日</a>&nbsp;|&nbsp;
		<a href="<%=request.getContextPath() %>/app/QiuBaiServlet?from=imgrank">真相</a>&nbsp;|&nbsp;
		<a href="<%=request.getContextPath() %>/app/QiuBaiServlet?from=late">最新</a>
	</div>
	<c:forEach items="${page.content}" var="content" varStatus="s">
		<div class="qiushi">
			<span>No.${(page.size*page.number)+(s.index+1)}&nbsp;</span>
	   		${content.contentMain }
	    	<a href="${content.contentImg }">
	    		<img src="${content.contentImg }" />
	    	</a>
		    <p class="vote">
		    	<img src="static/qiushi/good.gif?v=b990b" alt="支持" />
		    	<a id='u80962227'>${content.funnyNum}</a> 
		    	&nbsp;|&nbsp;
		    	<img src="static/qiushi/bad.gif?v=23486" alt="囧" />
		    	<a id='d80962227'>${content.notfunnyNum }</a> 
		    	&nbsp;|&nbsp;
		    	<a href="article/80962227">
		    		<strong>${content.commentNum}</strong>条评论
		    	</a>
		    </p>
	  	</div>
	</c:forEach>
</body>
</html>