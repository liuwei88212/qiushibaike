<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="description" content="糗事百科笑话段子分享社区,在这里你可以发现校园、办公室、家庭中正在发生的形形色色的搞笑糗事,分享身边有内涵的笑话故事。将无节操无底线进行到底。娱乐自己,放松大家,爆笑态度面对生活。" />
  <title>投稿</title>
  <script src="../js/jquery-1.7.2.min.js" type="text/javascript"></script>
  <link href="css/app.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript">
	  $(function(){
			//菜单初始化
			var from = "<%=request.getParameter("from")%>";
			$(".menu-bar .menu li").each(function(){
				var rel = $(this).children().first().attr("rel");
				if(from==rel){
					$(this).children().first().attr("id","highlight");
				}
			});
		});
  </script>
  <style>
  	body {padding-top: 0px !important;}
  </style>
</head>

<body>
  <!-- Header Start -->
  <div id="header" class="head">
    <div class="content-block">
      <div class="logo">
        <a href="">糗事百科</a>
      </div>

      <div class="userbar">
        <div class="login hidden">
          <a href="my" class="username" title="个人中心"></a><a href="logout" class="exit" title="退出">退出</a><a href=
          "my?messager=open" class="message" title="小纸条"><span id="unread_messages_count"></span></a><a href=
          "new4/invitation_codes" class="code" title="邀请码"></a>
        </div>

        <div class="logout">
          <a href="javascript:void(0);" class="fn-signin-required logintop" id='logintop'>登录</a>
        </div>
      </div>
    </div>
  </div><!-- Header End -->
  
  <!-- Menu Start -->
  <div id="menu" class="menu-bar clearfix">
    <div class="content-block">
      <div class="menu clearfix">
        <jsp:include page="../menu.ul.jsp" />
      </div>
    </div>
  </div><!-- Menu End --><!-- Main content Start -->

  <div id="content" class="main">
    <div class="content-block clearfix">
      <div class="clearfix mt-l mb-l p-xl b-w bs-l">
        <!-- 返回信息 -->
        
        <!-- 发表表单 -->
        <div class="post-readme wx250 f-r">
          <h3>投稿须知</h3>
          <ol>
            <li>自己的或朋友的糗事，真实有笑点，不含政治、色情、广告、诽谤、歧视等内容。</li>

            <li>糗事经过审核后发表。</li>

            <li>转载请注明出处。</li>

            <li>投稿内容著作权、版权归糗事百科网站所有。</li>
          </ol>
        </div>

        <div class="wx600 f-l">
          <form action="http://www.qiushibaike.com/add" enctype="multipart/form-data" id="new_article" method="post">
            <input type="hidden" id="article_group_id" name="article[group_id]" value="2" />
            <textarea id="qiushi_text" class="wx578 p-m fs-s b-f-g b-lg bsi-l" placeholder="分享一件新鲜事..." 
            	name="article[content]" rows="15">
			</textarea>
            <div class="clearfix mt-r1 mb-m p-m c-lg b-f-g b-lg">
              <div class="f-r" style="display:none;">
                <input type="hidden" name="article[anonymous]" value="0" /><input name="article[anonymous]" type="checkbox" id=
                "article_anonymous" onclick="this.value=this.checked?&#39;1&#39;:&#39;0&#39;" value="0" />匿名投稿
              </div>

              <div class="f-l">
                <label>照片:</label><input type="file" id="article_picture" name="article[picture]" />
              </div>
            </div>

            <div id="length" class="f-r"></div>
            <!--字数统计-->
            <input type="hidden" name="article[comment_status]" value="open" />
            
            <!--允许评论-->
            <button type="submit" class="p-xl ptb-m b-g fs-s c-w br-s bs-l" id="article_submit" 
            	name="commit">投递</button>
          </form>
        </div>
      </div>
    </div>
  </div><!-- Main content end --><!-- Footer Start -->

  <div id="footer" class="foot">
    <!-- <div id="tanx-a-mm_31442535_2978450_13730393"><script charset="gbk" src="http://p.tanx.com/ex?i=mm_31442535_2978450_13730393"></script></div> -->

    <div class="footer-links clearfix">
      <div class="copyrights">
        <div class="copyrights-links clearfix">
          <a href="http://about.qiushibaike.com/jobs.html" target="_blank">招聘职位</a><a href=
          "http://about.qiushibaike.com/feedback.html" target="_blank">意见反馈</a><a href="http://app.qiushibaike.com" target=
          "_blank">客户端</a><a href=
          "http://shang.qq.com/wpa/qunwpa?idkey=59e0270d94745f2c8735c2836aa846531a1bacad12ddde282983f7482cd5b55c" target=
          "_blank">官方QQ群</a><a href="http://qxy.qiushibaike.com/" target="_blank">糗西游</a>
        </div>

        <p class="copyright">ɠqiushibaike.com 京ICP备14028348号<br />
        友际无限（北京）科技有限公司 | 北京市朝阳区慧忠里洛克时代A座6A06 | 010-84872896</p>
      </div>

      <div class="socials">
        <ul class="social-btns clearfix">
          <li>关注：</li>
          <!-- <li><a href="http://about.qiushibaike.com/about.html" target="_blank"><i class="iconfont">&#xf004e;</i></a></li> -->

          <li>
            <a href="http://user.qzone.qq.com/1492495058" class="social-qzone" target="_blank"><i class="iconfont">P</i></a>
          </li>

          <li>
            <a href="http://t.qq.com/qiushibaike" class="social-tqq" target="_blank"><i class="iconfont">O</i></a>
          </li>

          <li>
            <a href="http://weibo.com/qiushibaike" class="social-weibo" target="_blank"><i class="iconfont">L</i></a>
          </li>

          <li>
            <a href="http://mail.qq.com/cgi-bin/bookcol?colid=314" class="social-rss" target="_blank"><i class="iconfont">Q</i></a>
          </li>
        </ul>
      </div>
    </div>
  </div><!-- Footer End -->
</body>
</html>
