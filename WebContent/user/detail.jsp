<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${user.userName }个人空间</title>
    <link href="<%=request.getContextPath() %>/user/css/uc.app.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=request.getContextPath() %>/user/css/messager.app.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=request.getContextPath() %>/user/css/layout.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <!-- #container -->
    <div id="container">
      <!-- top bar -->
      <div id="topbar" class="topbar">
        <div class="container clearfix">
          <div id="toplogo" class="logo">
            <h1 class="heading-logo">
              <a href="<%=request.getContextPath() %>/">
              <i class="iconfont"> </i>糗事百科</a>
            </h1>
          </div>
          <div id="global-nav" class="topnav clearfix">
            <div id="signin" class="signin">
              <a href="javascript:void(0);" class="fn-signin-required" data-go="window" style="border: none;">登录</a>
              <a href="new4/signup">注册</a>
            </div>
          </div>
        </div>
      </div>
      <!-- center-container -->
      <div id="center-container" class="container clearfix">
        <!-- left -->
        <div id="left" class="sidebar clearfix">
          <div class="userinfo sidebar-module clearfix">
            <div class="avatar-cover clearfix">
              <a href="users/11968255">
                <img alt="${user.userName }" class="avatar"
                src="${user.userHead }" />
              </a>
            </div>
            <h3 class="username-heading">${user.userName }</h3>
            <div class="button-list clearfix">
              <a href="users/11968255/follow" class="follow">关注</a>
              <a href="javascript:void(0);" class="message send-message-to fn-signin-required" data-target="11968255"
              	data-nickname="--等等。" data-avatar="20140527205610.jpg">
              		发小纸条
              </a>
            </div>
          </div>
        </div>
        <!-- right -->
        <div id="right" class="main clearfix">
          <div id="sub-nav" class="sub-nav clearfix">
            <a href="users/11968255" class="current">糗事</a>
            <a href="users/11968255/comments" class="last">评论</a>
          </div>
          <c:forEach items="${page.content}" var="content" varStatus="s">
	          <div id="article82978727" class="qiushi_body article clearfix">
	            <p style="margin-bottom: 15px; margin-top: 0px;">
	            	${content.contentMain }
	            </p>
	            <div class="counts clearfix">
	              <span id="pos-score-82978727" class="up">
	              <i class="iconfont"></i>${content.funnyNum } 个顶</span>
	              <span class="comments">
	                <a href="article/82978727" class="comments" id="c-82978727" target="_blank">
	                <i class="iconfont"></i>${content.commentNum } 条评论</a>
	              </span>
	            </div>
	          </div>
          </c:forEach>
        </div>
      </div>
    </div>
  </body>
</html>
