<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Window-target" content="_top" />
<meta name="author" content="BruceLiu" />
<meta name="description" content="糗事百科个人版-by BruceLiu" />
<meta name="keywords" content="糗事百科,糗事百科个人版,BruceLiu,布鲁斯刘" />
<meta name="robots" content="all" /> 
<link href="<%=request.getContextPath() %>/css/layout.menu.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/layout.inspect.css" rel="stylesheet" type="text/css" />
<script src="<%=request.getContextPath() %>/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/NeuF.ScrollPage.js" type="text/javascript"></script>
<title>【糗事百科-个人版】 审核</title>
<script type="text/javascript">
	$(function(){
		//菜单初始化
		var from = "<%=request.getAttribute("from")%>";
		$(".menu li").each(function(){
			var rel = $(this).children().first().attr("rel");
			if(from==rel){
				$(this).children().first().attr("id","highlight");
			}
		});
	});
	/**滚动加载 */
	$(function () {
	    window.scrollTo(0, 0); //每次F5刷新把滚动条置顶
	    //marginBottom表示滚动条离底部的距离，0表示滚动到最底部才加载，可以根据需要修改  
	    new NeuF.ScrollPage(window, { delay: 1000, marginBottom: 0 }, function (offset) {
	        if (offset > 0) {
	            $("#Loadding").show(); //加载提示
	            setTimeout(function () {
	                var pageNum = $(".loadContent").size() + 1;
	                var load = $("<div class='loadContent'></div>").load("NewsController?method=load&pageNum="+pageNum);
	                $("#content-left").append(load);
	                $("#Loadding").hide();
	            }, 1000);
	        }
	    });
	});
	
	function vote(userId,data){
		var url = "http://webinsp.qiushibaike.com/new3/inspect2/"+userId+"/"+data+"?time=" + (new Date().getTime()/ 1000);
		document.getElementById('myfarme').src = url;
		
		var jsUrl = "http://webinsp.qiushibaike.com/static/js/inspect.js";
		loadScript(jsUrl);
		/**
		$.ajax({
		    url: url,//跨域的dns/index!searchJSONResult.action,
		    type: "GET",
		    dataType: 'jsonp',
		    data: "",
		    timeout: 5000,
		    success: function (json) {//客户端jquery预先定义好的callback函数,成功获取跨域服务器上的json数据后,会动态执行这个callback函数
		        alert(json);
		    },
		    error: function (xhr, ajaxOptions, thrownError){
		        alert("Http status: " + xhr.status + " " + xhr.statusText + "\najaxOptions: " + ajaxOptions + "\nthrownError:"+thrownError + "\n" +xhr.responseText);
		    }
		});
		*/
	}
</script>
<style type="text/css">
	div#Loadding {margin-top: 10px; display: none; font-weight: bold; color:Red; }
	.menu-bar-fixed .menu ul{text-align: center;margin-left: 220px;} 
</style>
</head>
<body>
	<div class="menu-bar-fixed" id="navFixed" style="width: 100%">
		<div class="menu" align="center">
			<jsp:include page="menu.ul.jsp"/>
		</div>
		<div class="userbar">
			<div class="login" style="display: block;">
				<a title="个人中心" class="username" href="/my">转角遇见你妹</a>
				<a title="退出" class="exit" href="/logout">退出</a>
				<a title="小纸条" class="message" href="/my?messager=open">
					<span class="bti"></span>
					<span id="unread_messages_count" style="display: none;"></span>
				</a>
			</div>
			<div class="logout" style="display: none;">
				<a id="logintop" class="fn-signin-required logintop" href="javascript:void(0);">登录</a>
			</div>
		</div>
	</div>
	<div class="main" id="content-left">
		<h2 style="margin: 40px auto;">我觉得吧，这件糗事...</h2>
		<div class="loadContent">
			<c:forEach items="${page.content}" var="news" varStatus="s">
				<div class="main">
					<div class="content">
						<p id="content">
							<span>No.${(page.size*page.number)+(s.index+1)}&nbsp;</span>
							${news.content }
						</p>
						<a id="pic">${news.img }</a>
						<p id="tag">${news.tags }</p>
					</div>
					<div class="option">
						<a class="up voteme" onclick="vote(${news.userId},10000)"><span></span>好笑</a> 
						<a class="down voteme" onclick="vote(${news.userId},-10000)"><span></span>不给力</a> 
						<a class="report voteme" onclick="vote(${news.userId},-20000)"><span></span>滚粗</a> 
					</div>
				</div>
			</c:forEach>
		</div>
		<div id="Loadding" class="main">
	               正在加载，请稍候 ...
		</div>
	</div>
	
	<jsp:include page="footer.jsp"/>
	
	
	<div class="feedback">
		<a id="a-suggest" href="#">给点建议</a>
	</div>
	<div id="feedback" class="feedback-box fbox">
		<a class="close" href="#">X</a>
		<p>请帮助我们进步,请留下联系方式哟</p>
		<form id="form-suggest" method="post" action="/new4/suggest">
			<textarea tabindex="1" name="suggestion" id="suggest-text"
				type="text"></textarea>
			<button type="submit">提交</button>
		</form>
	</div>
</body>
</html>